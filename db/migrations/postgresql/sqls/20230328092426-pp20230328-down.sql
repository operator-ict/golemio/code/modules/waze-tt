-- v_sck_route_live source

CREATE OR REPLACE VIEW analytic.v_sck_route_live
AS
SELECT
        wrl.route_id       ,
        wrl.update_time    ,
        wrl."time"         ,
        wrl.length         ,
        wrl.historic_time  ,
        wrl.jam_level      ,
        wrl.create_batch_id,
        wrl.created_at     ,
        wrl.created_by     ,
        wrl.update_batch_id,
        wrl.updated_at     ,
        wrl.updated_by
FROM
        wazett.wazett_route_lives wrl
WHERE
        (
                wrl.route_id IN
                (
                        SELECT DISTINCT
                                vsrd.id
                        FROM
                                analytic.v_sck_route_details vsrd))
AND     wrl.update_time::double precision >= (date_part('epoch'::text, CURRENT_DATE - '7 days'::interval) * 1000::double precision);