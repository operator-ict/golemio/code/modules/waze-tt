CREATE SCHEMA IF NOT EXISTS analytic;

-- analytic.lkpr_dashboard_route_names definition


CREATE TABLE analytic.lkpr_dashboard_route_names (
	route_type text NULL,
	route_num text NULL,
	route_group_name text NULL,
	order_idx int4 NULL
);

INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '01', 'Podbaba => Evropská => letiště', 10);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '02', 'Vítězné nám. => Evropská => letiště', 9);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '03', 'Letenské náměstí => Hradčanská => Evropská => letiště', 7);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '04', 'Letenské náměstí => Bubeneč => Evropská => letiště', 8);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '05', 'Smíchov => Horní Liboc => letiště', 3);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '06', 'Motol => Horní Liboc => letiště', 2);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '07', 'Letenské náměstí => Karlovarská => letiště', 6);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '08', 'Smíchov => Karlovarská => letiště', 4);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '09', 'Klárov => Karlovarská => letiště', 5);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '10', 'Zličín/Řepy => Drnovská => letiště', 1);
INSERT INTO analytic.lkpr_dashboard_route_names (route_type, route_num, route_group_name, order_idx) VALUES('TN', '11', 'Podbaba => Horoměřice => letiště', 11);

-- analytic.barrande_black_list_route definition

CREATE TABLE analytic.barrande_black_list_route (
	route_id int8 NOT NULL,
	note varchar(250) NULL,
	created_at timestamptz NULL DEFAULT now(),
	CONSTRAINT barrande_black_list_route_pkey PRIMARY KEY (route_id)
);

INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(1652108561613, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(1652106375503, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(1652096816287, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32477, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32496, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32497, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33144, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(19671439451, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32437, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33057, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(11231032274, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33140, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(12130815233, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33056, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(11489476203, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33139, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(13267551138, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33138, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(18029801972, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32896, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32876, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(11113356540, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(33083, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32988, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(11605358669, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32594, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32502, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32598, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32886, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(32409, NULL, '2022-05-26 20:37:19.412');
INSERT INTO analytic.barrande_black_list_route (route_id, note, created_at) VALUES(11211603747, 'Nahrazeno narovnanou trasou 1653666818530, ukončena uzavírka (pan Přemek)', '2022-06-01 09:27:21.273');

-- analytic.waze_dashboard_route_name definition

CREATE TABLE analytic.waze_dashboard_route_name (
	route_id int4 NOT NULL,
	idx int4 NULL,
	"from" varchar(100) NULL,
	"to" varchar(100) NULL,
	db_name varchar(100) NULL,
	"name" varchar(500) NULL,
	CONSTRAINT waze_dashboard_route_name_pkey PRIMARY KEY (route_id)
);

INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22690, 1, '5. května', 'Bubenská', 'Legerova, Wilsonova Praha', '01a. Z ul. 5. května přes Legerovu a Wilsonovu k Bubenské ul. (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22691, 2, 'Bubenská', '5. května', 'Wilsonova Praha', '01b. Z Bubenské ul. přes Wilsonovu a Legerovu k ul. 5. května (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22697, 3, 'Resslova', 'Jugoslávská', 'Resslova, Ječná Praha', '02a. Od Jiráskova mostu => I.P. Pavlova - z ul. Resslova do ul. Jugoslávská (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22733, 4, 'Anglická', 'Žitná', 'Anglická, Žitná Praha', '02b. Z nám. Míru => ul. Anglická => ul. Žitná => Karlovo náměstí (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22732, 5, 'Národní', 'Národní', 'Národní Praha V-Z', '03a. Ulice Národní od OD Máj po Národní divadlo (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22731, 6, 'Národní', 'Národní', 'Národní Praha Z-V', '03b. Ulice Národní od Národního divadla po OD Máj (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22716, 7, 'Újezd', 'Letenská', 'Újezd, Karmelitská, Letenská Praha', '04a. Újezd => Malostranské nám. => ul. Letenská => Klárov (J-S)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22715, 8, 'Valdštejnská', 'Újezd', 'Valdštejnská, Malostranské náměstí, Karmelitská Praha', '04b. Klárov => ul. Valdštejnská => Malostranské nám. => Újezd (S-J)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22720, 9, 'Masarykovo nábřeží', 'náměstí Curieových', 'Divadelní, Smetanovo nábřeží, 17. listopadu Praha', '05a. Masarykovo náb. => ul. Divadelní => Smetanovo náb. =>  ul. 17. listopadu (J-S)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22754, 10, 'Dvořákovo nábřeží', 'Smetanovo nábřeží', 'Smetanovo nábřeží Praha', '05b. Z Dvořákova nábřeží => Smetanovo náb. => po Národní divadlo (S-J)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22729, 11, 'nábřeží Edvarda Beneše', 'nábřeží Edvarda Beneše', 'nábřeží Edvarda Beneše Praha Z-V', '06a. Náb. Edvarda Beneše od Čechova mostu po Štefánikův most (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22728, 12, 'nábřeží Edvarda Beneše', 'nábřeží Edvarda Beneše', 'nábřeží Edvarda Beneše Praha V-Z', '06b. Náb. Edvarda Beneše od Štefánikova mostu po Čechův most (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22730, 13, 'nábřeží Kapitána Jaroše', 'Argentinská', 'nábřeží Kapitána Jaroše, Bubenské nábřeží Praha', '07a. Z nábřeží Kapitána Jaroše (Hlávkův most) do ul. Argentinská (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22713, 14, 'Chotkova', 'Badeniho', 'Chotkova, Badeniho Praha', '08a. Z Klárova do ul. Badeniho (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22714, 15, 'Badeniho', 'Klárov', 'Badeniho, Chotkova Praha', '08b. Z ul. Badeniho na Klárov (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22725, 16, 'Milady Horákové', 'Milady Horákové', 'Milady Horákové Praha V-Z', '09a. Od ul. Badeniho => ul. Milady Horákové => po Prašný most (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22724, 17, 'Milady Horákové', 'Milady Horákové', 'Milady Horákové Praha Z-V', '09b. Z Prašného mostu => ul. Milady Horákové => po ul. Badeniho (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22723, 18, 'Milady Horákové', 'Patočkova', 'Milady Horákové, Patočkova Praha', '10a. Z Prašného mostu na ul. Patočkova (po Malovanku) (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22722, 19, 'Patočkova', 'Milady Horákové', 'Patočkova, Milady Horákové Praha', '10b. Z ul. Patočkova (od Malovanky) na Prašný most (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22696, 20, 'Pod stadiony', 'Vaníčkova', 'Pod stadiony, Vaníčkova Praha', '11a. Smíchov => Strahov => Malovanka - z ul. Pod stadiony do ul. Vaníčkova (J-S)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22695, 21, 'Vaníčkova', 'Pod stadiony', 'Vaníčkova Praha', '11b. Malovanka => Strahov => Smíchov - z ul. Vaníčkova do ul. Pod stadiony (S-J)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22705, 22, 'Holečkova', 'Zapova', 'Holečkova Praha', '12a. Z ul. Holečkova do ul. Zapova (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22706, 23, 'Zapova', 'Holečkova', 'Holečkova Praha', '12b. Z ul. Zapova do ul. Holečkova (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22703, 24, 'Plzeňská', 'Plzeňská', 'Plzeňská Praha', '13a. Z ul. Kartouzská do ul. Plzeňská (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22704, 25, 'Vrchlického', 'Duškova', 'Vrchlického, Duškova Praha', '13b. Z ul. Vrchlického do ul. Duškova (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22711, 26, 'Dienzenhoferovy sady', 'Kartouzská', 'V botanice, Kartouzská Praha', '14a. Od Jiráskova mostu => ul. Kartouzská => po ul. Radlická (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22712, 27, 'Vltavská', 'Vltavská', 'Vltavská Praha', '14b. Ulice Vltavská na Smíchově (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22702, 28, 'náměstí Jana Palacha', 'Klárov', 'Mánesův most, Klárov Praha', '15a. Mánesův most (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22701, 29, 'Klárov', 'náměstí Jana Palacha', 'Klárov, Mánesův most Praha', '15b. Mánesův most (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22709, 30, 'Jiráskův most', 'Jiráskův most', 'Jiráskův most Praha V-Z', '16a. Jiráskův most (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22710, 31, 'Jiráskův most', 'Jiráskův most', 'Jiráskův most Praha ZV', '16b. Jiráskův most (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22718, 32, 'Most legií', 'Vítězná', 'Most legií, Vítězná Praha', '17a. Most legií k Újezdu (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22717, 33, 'Vítězná', 'Most legií', 'Vítězná, Most legií Praha', '17b. Most legií z Újezdu (do centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22693, 34, 'Strakonická', 'V Holešovičkách', 'Bubenečský tunel Praha', '18a. Tunely Zlíchov => Strahov => Blanka - ze Strakonické do ul. V Holešovičkách (J-S)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(22694, 35, 'V Holešovičkách', 'Strakonická', 'Bubenečský tunel Praha', '18b. Tunely Blanka => Strahov => Zlíchov - z ul. V Holešovičkách na Strakonickou (S-J)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(24260, 36, 'Evropská', 'Aviatická', 'Evropská - Letiště', '19a. Vítězné nám. => Evropská => LKPR (z centra)');
INSERT INTO analytic.waze_dashboard_route_name (route_id, idx, "from", "to", db_name, "name") VALUES(24261, 37, 'Aviatická', 'Svatovítská', 'Letiště - Svatovítská', '19b. Z LKPR => ul. Evropská => ul. Velvarská => ul. Gen. Píky => Prašný most (do centra)');


-- analytic.v_barrandov_bridge_route_details source

CREATE OR REPLACE VIEW analytic.v_barrandov_bridge_route_details
AS SELECT re.id,
    re.name,
    re.section,
    re.reverse_direction,
    re.from_name,
    re.to_name,
    re.poradi
   FROM ( SELECT r.id,
            r.name,
                CASE
                    WHEN r.name ~~ '% ÚS %'::text THEN true
                    WHEN r.name ~~ '% TN %'::text THEN false
                    ELSE NULL::boolean
                END AS section,
                CASE
                    WHEN r.name ~~ '% OS%'::text THEN true
                    ELSE false
                END AS reverse_direction,
            r.from_name,
            r.to_name,
            row_number() OVER (PARTITION BY r.name ORDER BY r.id DESC) AS poradi
           FROM wazett_routes r
          WHERE "left"(r.name, 2) = 'BM'::text) re;

-- analytic.v_lkpr_route_details source

CREATE OR REPLACE VIEW analytic.v_lkpr_route_details
AS SELECT re.id,
    re.name,
    re.route_code,
    re.route_name,
    re.from_name,
    re.to_name,
    re.route_type,
    re.route_num,
    re.route_variant,
        CASE
            WHEN re.route_variant = 'A1'::text THEN 'Na letiště'::text
            WHEN re.route_variant = 'B1'::text THEN 'Z letiště'::text
            ELSE 'Ostatní'::text
        END AS direction,
    drn.route_group_name,
    drn.order_idx
   FROM ( SELECT rc.id,
            rc.name,
            rc.route_code,
            rc.route_name,
            rc.from_name,
            rc.to_name,
            split_part(rc.route_code, '-'::text, 1) AS route_type,
            split_part(rc.route_code, '-'::text, 2) AS route_num,
            split_part(rc.route_code, '-'::text, 3) AS route_variant
           FROM ( SELECT r.id,
                    r.name,
                    split_part(r.name, ' '::text, 1) AS route_code,
                    "substring"(r.name, "position"(r.name, ' '::text) + 1) AS route_name,
                    r.from_name,
                    r.to_name
                   FROM wazett_routes r
                  WHERE r.feed_id = 1) rc) re
     LEFT JOIN analytic.lkpr_dashboard_route_names drn ON re.route_type = drn.route_type AND re.route_num = drn.route_num
  ORDER BY drn.order_idx;

-- analytic.v_barrande_route source

CREATE OR REPLACE VIEW analytic.v_barrande_route
AS SELECT DISTINCT ON (wazett_routes.name) wazett_routes.name,
        CASE
            WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN true
            ELSE false
        END AS is_trasa,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN true
            ELSE false
        END AS is_usek,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN true
            ELSE false
        END AS is_opacny_smer,
        CASE
            WHEN wazett_routes.id = ANY (ARRAY[32875, 32876]) THEN 'OICT 3 DO'::text
            WHEN wazett_routes.id = ANY (ARRAY[32888, 32889]) THEN 'OICT 3 OD'::text
            ELSE "substring"(wazett_routes.name, 4,
            CASE
                WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN "position"(wazett_routes.name, ' TN'::text) - 3
                WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) - 3
                ELSE NULL::integer
            END)
        END AS name_trasa,
    "substring"(wazett_routes.name,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) + 4
            ELSE length(wazett_routes.name) + 1
        END,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN "position"(wazett_routes.name, ' OS'::text) - "position"(wazett_routes.name, ' ÚS'::text) - 4
            ELSE length(wazett_routes.name) - "position"(wazett_routes.name, ' ÚS'::text)
        END) AS name_usek
   FROM wazett_routes
  WHERE "left"(wazett_routes.name, 2) = 'BM'::text AND NOT (EXISTS ( SELECT 1
           FROM analytic.barrande_black_list_route bl
          WHERE bl.route_id = wazett_routes.id))
  ORDER BY wazett_routes.name, wazett_routes.created_at DESC;

-- analytic.v_wazett_routes_lines source

CREATE OR REPLACE VIEW analytic.v_wazett_routes_lines
AS SELECT wazett_routes.id,
    wazett_routes.name,
    st_astext(wazett_routes.line) AS line,
    concat('#', "left"(lpad(to_hex((wazett_routes.id::double precision * character_length(wazett_routes.name)::double precision / (( SELECT max(wazett_routes_1.id * character_length(wazett_routes_1.name)) AS max
           FROM wazett_routes wazett_routes_1))::double precision * 10000000::double precision)::bigint), 6, '0'::text), 6)) AS color
   FROM wazett_routes;

-- analytic.v_barrandov_bridge_route_live source

CREATE OR REPLACE VIEW analytic.v_barrandov_bridge_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett_route_lives wrl
  WHERE (wrl.route_id IN ( SELECT DISTINCT wazett_routes.id
           FROM wazett_routes
          WHERE wazett_routes.name ~~ 'BM %'::text)) AND wrl.update_time::double precision >= (date_part('epoch'::text, CURRENT_DATE - '7 days'::interval) * 1000::double precision);


-- analytic.v_route_live source

CREATE OR REPLACE VIEW analytic.v_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett_route_lives wrl
  WHERE (wrl.route_id = ANY (ARRAY[24949, 24953, 24962, 24924, 24925, 24950, 24951, 24952, 24954, 24955, 24956, 24957, 24958, 24960, 24961, 24963, 24964, 24965, 24966, 24967, 24968, 24969])) AND wrl.update_time::double precision >= (( SELECT max(wazett_route_lives.update_time)::double precision - date_part('epoch'::text, '1 year'::interval) * 1000::double precision AS max
           FROM wazett_route_lives));

-- analytic.v_instatntomtom_days source

CREATE OR REPLACE VIEW analytic.v_instatntomtom_days
AS SELECT to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text) AS day_index,
    wrl.route_id,
    round(avg(wrl."time"::numeric / wrl.historic_time::numeric), 2) AS t_index_value,
    count(*) AS t_index_count
   FROM wazett_route_lives wrl
  WHERE to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone >= '05:00:00'::time without time zone AND to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone <= '21:00:00'::time without time zone
  GROUP BY (to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text)), wrl.route_id;

-- analytic.v_route_travel_times source

CREATE OR REPLACE VIEW analytic.v_route_travel_times
AS SELECT ts.route_id,
    ts.year,
    ts.month,
    ts.day,
    ts.dow,
    ts.hour,
    ts.quarter,
    ts.hour + (ts.quarter::numeric / 60::numeric)::double precision AS hour_quarter,
    ((((((((ts.year || '-'::text) || ts.month) || '-'::text) || ts.day) || ' '::text) || ts.hour) || ':'::text) || ts.quarter)::timestamp without time zone AS date,
    avg(ts.travel_time)::integer AS travel_time
   FROM ( SELECT raw.route_id,
            raw.update_time,
            raw.travel_time,
            date_part('year'::text, raw.update_time) AS year,
            date_part('month'::text, raw.update_time) AS month,
            date_part('day'::text, raw.update_time) AS day,
            date_part('dow'::text, raw.update_time) AS dow,
            date_part('hour'::text, raw.update_time) AS hour,
            date_part('minute'::text, raw.update_time) AS minute,
            date_part('minute'::text, raw.update_time)::integer / 15 * 15 AS quarter
           FROM ( SELECT wazett_route_lives.route_id,
                    timezone('Europe/Prague'::text, to_timestamp((wazett_route_lives.update_time / 1000)::double precision)::timestamp without time zone) AS update_time,
                    wazett_route_lives."time" AS travel_time
                   FROM wazett_route_lives
                     JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = wazett_route_lives.route_id) raw) ts
  WHERE ts.year > 2000::double precision
  GROUP BY ts.route_id, ts.year, ts.month, ts.day, ts.dow, ts.hour, ts.quarter;

-- analytic.v_barrande_last_update source

CREATE OR REPLACE VIEW analytic.v_barrande_last_update
AS SELECT to_timestamp((wrl.update_time / 1000)::double precision) AS last_update
   FROM wazett_route_lives wrl
     LEFT JOIN wazett_routes wr ON wrl.route_id = wr.id
  WHERE "left"(wr.name, 3) = 'BM '::text
  ORDER BY wrl.update_time DESC
 LIMIT 1;




-- public.v_lkpr_export source

CREATE OR REPLACE VIEW public.v_lkpr_export
AS SELECT COALESCE(vrd.id, wrl.route_id) AS route_id,
    vrd.name,
    vrd.route_name,
    vrd.from_name,
    vrd.to_name,
    vrd.route_num,
    vrd.direction,
    vrd.order_idx,
    to_timestamp((wrl.update_time / 1000)::double precision) AS update_time,
    wrl."time" AS actual_time,
    round(wrl.length::numeric * 3.6 / wrl."time"::numeric) AS actual_speed,
    wrl.historic_time,
    round(wrl.length::numeric * 3.6 / wrl.historic_time::numeric) AS historic_speed,
    wrl.length
   FROM analytic.v_lkpr_route_details vrd
     FULL JOIN wazett_route_lives wrl ON vrd.id = wrl.route_id
  WHERE vrd.name ~~ 'TN-%'::text OR vrd.route_code ~~ 'TN-%'::text;

-- analytic.v_barrande_route_hour_core source

CREATE OR REPLACE VIEW analytic.v_barrande_route_hour_core
AS SELECT wr.name,
    to_timestamp((rl.update_time / 1000)::double precision)::date AS datum,
    date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) AS hodina,
    (to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) + 1::double precision, '09'::text) AS cas_interval,
    round(avg(rl."time"), 2) AS avg_time,
    round(avg(rl.length * 1000 / (rl."time" * 360)), 2) AS avg_speed
   FROM wazett_route_lives rl
     JOIN wazett_routes wr ON wr.id = rl.route_id
  WHERE rl.length > 0 AND "left"(wr.name, 2) = 'BM'::text AND NOT (EXISTS ( SELECT 1
           FROM analytic.barrande_black_list_route bl
          WHERE bl.route_id = rl.route_id))
  GROUP BY wr.name, (to_timestamp((rl.update_time / 1000)::double precision)::date), (date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)));




-- analytic.mv_barrande_normal source

CREATE MATERIALIZED VIEW analytic.mv_barrande_normal
TABLESPACE pg_default
AS SELECT vtbh.name,
    vtbh.hodina,
    round(avg(vtbh.avg_time), 2) AS avg_time,
    round(avg(vtbh.avg_speed)) AS avg_speed
   FROM analytic.v_barrande_route_hour_core vtbh
  WHERE vtbh.datum >= '2022-04-24'::date AND vtbh.datum <= '2022-05-05'::date AND (date_part('dow'::text, vtbh.datum) = ANY (ARRAY[2::double precision, 3::double precision, 4::double precision]))
  GROUP BY vtbh.name, vtbh.hodina
WITH DATA;

-- View indexes:
CREATE INDEX mv_barrande_normal_idx ON analytic.mv_barrande_normal USING btree (name, hodina);

 -- analytic.v_barrande_route_hour source

CREATE OR REPLACE VIEW analytic.v_barrande_route_hour
AS SELECT p.name,
    p.datum,
    p.hodina,
    p.cas_interval,
    p.avg_time,
    p.avg_speed,
    n.avg_time AS normal_avg_time,
    n.avg_speed AS normal_avg_speed
   FROM analytic.v_barrande_route_hour_core p
     LEFT JOIN analytic.mv_barrande_normal n ON n.name = p.name AND p.hodina = n.hodina;

-- analytic.v_barrande_union_time_interval source

CREATE OR REPLACE VIEW analytic.v_barrande_union_time_interval
AS WITH hodiny AS (
         SELECT brh.name,
            brh.datum,
            brh.hodina,
            brh.cas_interval,
            brh.avg_time,
            brh.avg_speed,
            brh.normal_avg_time,
            brh.normal_avg_speed
           FROM analytic.v_barrande_route_hour brh
          WHERE brh.datum < CURRENT_DATE OR brh.hodina < date_part('hour'::text, now())
        ), den AS (
         SELECT hodiny.name,
            hodiny.datum,
            '0 - 24h'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h15_18 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '15 -18'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 15::double precision AND hodiny.hodina <= 17::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 17::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h22_6 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '22 - 06'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE NOT (hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision) AND hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h6_22 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '06 - 22'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 21::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_10 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 10'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 7::double precision AND hodiny.hodina <= 9::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 10::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_11_15_19 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 11, 15 - 19'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE (hodiny.hodina >= 7::double precision AND hodiny.hodina <= 10::double precision OR hodiny.hodina >= 15::double precision AND hodiny.hodina <= 18::double precision) AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 18::double precision)
          GROUP BY hodiny.name, hodiny.datum
        )
 SELECT hodiny.name,
    hodiny.datum,
    hodiny.cas_interval,
    hodiny.avg_time,
    hodiny.avg_speed,
    hodiny.normal_avg_time,
    hodiny.normal_avg_speed
   FROM hodiny
UNION ALL
 SELECT den.name,
    den.datum,
    den.cas_interval,
    den.avg_time,
    den.avg_speed,
    den.normal_avg_time,
    den.normal_avg_speed
   FROM den
UNION ALL
 SELECT h15_18.name,
    h15_18.datum,
    h15_18.cas_interval,
    h15_18.avg_time,
    h15_18.avg_speed,
    h15_18.normal_avg_time,
    h15_18.normal_avg_speed
   FROM h15_18
UNION ALL
 SELECT h22_6.name,
    h22_6.datum,
    h22_6.cas_interval,
    h22_6.avg_time,
    h22_6.avg_speed,
    h22_6.normal_avg_time,
    h22_6.normal_avg_speed
   FROM h22_6
UNION ALL
 SELECT h6_22.name,
    h6_22.datum,
    h6_22.cas_interval,
    h6_22.avg_time,
    h6_22.avg_speed,
    h6_22.normal_avg_time,
    h6_22.normal_avg_speed
   FROM h6_22
UNION ALL
 SELECT h7_10.name,
    h7_10.datum,
    h7_10.cas_interval,
    h7_10.avg_time,
    h7_10.avg_speed,
    h7_10.normal_avg_time,
    h7_10.normal_avg_speed
   FROM h7_10
UNION ALL
 SELECT h7_11_15_19.name,
    h7_11_15_19.datum,
    h7_11_15_19.cas_interval,
    h7_11_15_19.avg_time,
    h7_11_15_19.avg_speed,
    h7_11_15_19.normal_avg_time,
    h7_11_15_19.normal_avg_speed
   FROM h7_11_15_19;

   -- analytic.v_barrande_data_all source

CREATE OR REPLACE VIEW analytic.v_barrande_data_all
AS WITH data1 AS (
         SELECT trasy.name,
            trasy.name_trasa,
            trasy.name_usek,
                CASE
                    WHEN trasy.is_opacny_smer THEN 'Zpět'::text
                    ELSE 'Tam'::text
                END AS smer,
            vbu.datum,
            vbu.cas_interval,
            vbu.avg_time,
            vbu.avg_speed,
            vbu.normal_avg_time,
            vbu.normal_avg_speed
           FROM analytic.v_barrande_union_time_interval vbu
             JOIN analytic.v_barrande_route trasy ON trasy.name = vbu.name
        ), data2 AS (
         SELECT data1.name_trasa,
            data1.name_usek,
            'Oba'::text AS smer,
            data1.datum,
            data1.cas_interval,
            sum(data1.avg_time) AS avg_time,
            avg(data1.avg_speed) AS avg_speed,
            sum(data1.normal_avg_time) AS normal_avg_time,
            avg(data1.normal_avg_speed) AS normal_avg_speed
           FROM data1
          GROUP BY data1.name_trasa, data1.name_usek, 'Oba'::text, data1.datum, data1.cas_interval
        )
 SELECT data1.name,
    data1.name_trasa,
    data1.name_usek,
    data1.smer,
    data1.datum,
    data1.cas_interval,
    data1.avg_time,
    data1.avg_speed,
    data1.normal_avg_time,
    data1.normal_avg_speed
   FROM data1
UNION
 SELECT vbt.name,
    data2.name_trasa,
    data2.name_usek,
    data2.smer,
    data2.datum,
    data2.cas_interval,
    data2.avg_time,
    data2.avg_speed,
    data2.normal_avg_time,
    data2.normal_avg_speed
   FROM data2
     JOIN analytic.v_barrande_route vbt ON vbt.name_trasa = data2.name_trasa AND vbt.name_usek = data2.name_usek AND NOT vbt.is_opacny_smer;




CREATE OR REPLACE PROCEDURE analytic.update_barande()
 LANGUAGE plpgsql
AS $procedure$
declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select
		case
			when start_day is not null
			then start_day
			else '2021-01-01'
		end as start_day_from into lastupdatetimestamp
	from (select max(datum)-interval '1 days' start_day from analytic.barrande_route_hour_core) barandemax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;


	insert into analytic.barrande_route_hour_core
	SELECT
		wr.name,
		to_timestamp((rl.update_time / 1000)::double precision)::date AS datum,
		date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) AS hodina,
		(to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) + 1::double precision, '09'::text) AS cas_interval,
		round(avg(rl."time"), 2) AS avg_time,
		round(avg(rl.length * 1000 / (rl."time" * 360)), 2) AS avg_speed
	FROM wazett_route_lives rl
		JOIN wazett_routes wr ON wr.id = rl.route_id
	WHERE
		rl.update_time > lastupdateunix and
	rl.length > 0 AND "left"(wr.name, 2) = 'BM'::text AND NOT (EXISTS ( SELECT 1
          FROM analytic.barrande_black_list_route bl
          WHERE bl.route_id = rl.route_id))
	GROUP BY wr.name, (to_timestamp((rl.update_time / 1000)::double precision)::date), (date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)))
	ON CONFLICT (name,datum,hodina,cas_interval)
		DO update
		SET avg_time = EXCLUDED.avg_time,
		avg_speed = EXCLUDED.avg_speed;


	insert into	analytic.barrande_data_all
	WITH
		v_barrande_route_hour as (
	SELECT p.name,
    p.datum,
    p.hodina,
    p.cas_interval,
    p.avg_time,
    p.avg_speed,
    n.avg_time AS normal_avg_time,
    n.avg_speed AS normal_avg_speed
   FROM analytic.barrande_route_hour_core p
     LEFT JOIN analytic.mv_barrande_normal n ON n.name = p.name AND p.hodina = n.hodina
	where datum > lastupdatetimestamp
		),
-- konec v_barrande_route_hour
		v_barrande_union_time_interval as (
		WITH hodiny AS (
         SELECT brh.name,
            brh.datum,
            brh.hodina,
            brh.cas_interval,
            brh.avg_time,
            brh.avg_speed,
            brh.normal_avg_time,
            brh.normal_avg_speed
           FROM v_barrande_route_hour brh
          WHERE brh.datum < CURRENT_DATE OR brh.hodina < date_part('hour'::text, now())
        ), den AS (
         SELECT hodiny.name,
            hodiny.datum,
            '0 - 24h'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h15_18 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '15 -18'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 15::double precision AND hodiny.hodina <= 17::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 17::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h22_6 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '22 - 06'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE NOT (hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision) AND hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h6_22 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '06 - 22'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 21::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_10 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 10'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 7::double precision AND hodiny.hodina <= 9::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 10::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_11_15_19 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 11, 15 - 19'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE (hodiny.hodina >= 7::double precision AND hodiny.hodina <= 10::double precision OR hodiny.hodina >= 15::double precision AND hodiny.hodina <= 18::double precision) AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 18::double precision)
          GROUP BY hodiny.name, hodiny.datum
        )
 SELECT hodiny.name,
    hodiny.datum,
    hodiny.cas_interval,
    hodiny.avg_time,
    hodiny.avg_speed,
    hodiny.normal_avg_time,
    hodiny.normal_avg_speed
   FROM hodiny
UNION ALL
 SELECT den.name,
    den.datum,
    den.cas_interval,
    den.avg_time,
    den.avg_speed,
    den.normal_avg_time,
    den.normal_avg_speed
   FROM den
UNION ALL
 SELECT h15_18.name,
    h15_18.datum,
    h15_18.cas_interval,
    h15_18.avg_time,
    h15_18.avg_speed,
    h15_18.normal_avg_time,
    h15_18.normal_avg_speed
   FROM h15_18
UNION ALL
 SELECT h22_6.name,
    h22_6.datum,
    h22_6.cas_interval,
    h22_6.avg_time,
    h22_6.avg_speed,
    h22_6.normal_avg_time,
    h22_6.normal_avg_speed
   FROM h22_6
UNION ALL
 SELECT h6_22.name,
    h6_22.datum,
    h6_22.cas_interval,
    h6_22.avg_time,
    h6_22.avg_speed,
    h6_22.normal_avg_time,
    h6_22.normal_avg_speed
   FROM h6_22
UNION ALL
 SELECT h7_10.name,
    h7_10.datum,
    h7_10.cas_interval,
    h7_10.avg_time,
    h7_10.avg_speed,
    h7_10.normal_avg_time,
    h7_10.normal_avg_speed
   FROM h7_10
UNION ALL
 SELECT h7_11_15_19.name,
    h7_11_15_19.datum,
    h7_11_15_19.cas_interval,
    h7_11_15_19.avg_time,
    h7_11_15_19.avg_speed,
    h7_11_15_19.normal_avg_time,
    h7_11_15_19.normal_avg_speed
   FROM h7_11_15_19
		),
-----------------------
		data1 AS (
        SELECT
			trasy.name,
            trasy.name_trasa,
            trasy.name_usek,
			CASE
				WHEN trasy.is_opacny_smer THEN 'Zpět'::text
				ELSE 'Tam'::text
			END AS smer,
            vbu.datum,
            vbu.cas_interval,
            vbu.avg_time,
            vbu.avg_speed,
            vbu.normal_avg_time,
            vbu.normal_avg_speed
        FROM v_barrande_union_time_interval vbu
            JOIN analytic.v_barrande_route trasy ON trasy.name = vbu.name
        ),
		data2 AS (
        SELECT
			data1.name_trasa,
            data1.name_usek,
            'Oba'::text AS smer,
            data1.datum,
            data1.cas_interval,
            sum(data1.avg_time) AS avg_time,
            avg(data1.avg_speed) AS avg_speed,
            sum(data1.normal_avg_time) AS normal_avg_time,
            avg(data1.normal_avg_speed) AS normal_avg_speed
        FROM data1
        GROUP BY data1.name_trasa, data1.name_usek, 'Oba'::text, data1.datum, data1.cas_interval
        )
 SELECT data1.name,
    data1.name_trasa,
    data1.name_usek,
    data1.smer,
    data1.datum,
    data1.cas_interval,
    data1.avg_time,
    data1.avg_speed,
    data1.normal_avg_time,
    data1.normal_avg_speed
   FROM data1
UNION
 SELECT vbt.name,
    data2.name_trasa,
    data2.name_usek,
    data2.smer,
    data2.datum,
    data2.cas_interval,
    data2.avg_time,
    data2.avg_speed,
    data2.normal_avg_time,
    data2.normal_avg_speed
   FROM data2
     JOIN analytic.v_barrande_route vbt ON vbt.name_trasa = data2.name_trasa AND vbt.name_usek = data2.name_usek AND NOT vbt.is_opacny_smer
	on conflict (name,name_trasa,name_usek,smer,datum,cas_interval)
	do update
		SET avg_time = EXCLUDED.avg_time,
		avg_speed = EXCLUDED.avg_speed,
		normal_avg_time = EXCLUDED.normal_avg_time,
		normal_avg_speed = EXCLUDED.normal_avg_speed;
end;
$procedure$
;
