DROP VIEW analytic.v_barrandov_bridge_route_live;

CREATE OR REPLACE VIEW analytic.v_barrandov_bridge_route_live
AS SELECT wrl.route_id,
    to_timestamp((wrl.update_time / 1000)::double precision) AS update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett.wazett_route_lives wrl
  WHERE (wrl.route_id IN ( SELECT DISTINCT wazett_routes.id
           FROM wazett.wazett_routes
          WHERE wazett_routes.name ~~ 'BM %'::text)) AND wrl.update_time::double precision >= (date_part('epoch'::text, CURRENT_DATE - '7 days'::interval) * 1000::double precision);
          