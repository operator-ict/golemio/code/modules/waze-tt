CREATE TABLE wazett_jams_stats_old (
	feed_id int4 NOT NULL,
	update_time int8 NOT NULL,
	jam_level int4 NOT NULL,
	wazers_count numeric NULL,
	length_of_jams int4 NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_jams_stats_pkey PRIMARY KEY (feed_id, update_time, jam_level)
);

CREATE TABLE wazett_route_lives_old (
	route_id int8 NOT NULL,
	update_time int8 NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_route_lives_pkey PRIMARY KEY (update_time, route_id)
);
CREATE INDEX wazett_route_lives_old_idx ON wazett.wazett_route_lives_old USING btree (update_time);

CREATE TABLE wazett_subroute_lives_old (
	route_id int8 NOT NULL,
	subroute_line_md5 uuid NOT NULL,
	update_time int8 NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_subroute_lives_pk PRIMARY KEY (route_id, update_time, subroute_line_md5)
);
CREATE INDEX wazett_subroute_lives_old_idx ON wazett.wazett_subroute_lives_old USING btree (update_time);