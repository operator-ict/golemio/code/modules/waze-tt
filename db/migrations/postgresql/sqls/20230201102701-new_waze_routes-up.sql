-- create table for aggregated travel times from wazett data
CREATE TABLE IF NOT EXISTS analytic.waze_route_travel_times_agg (
	route_id int8 NOT NULL,
	"year" float8 NOT NULL,
	"month" float8 NOT NULL,
	"day" float8 NOT NULL,
	dow float8 NOT NULL,
	"hour" float8 NOT NULL,
	quarter int4 NOT NULL,
	hour_quarter float8 NOT NULL,
	"date" timestamp NULL,
	travel_time int4 NULL,
	CONSTRAINT waze_route_travel_times_agg_pkey PRIMARY KEY (route_id, year, month, day, dow, hour, quarter, hour_quarter) INCLUDE (travel_time)
);

-- this procedure is used to update the above table
CREATE OR REPLACE PROCEDURE analytic.update_waze()
 LANGUAGE plpgsql
AS $procedure$

declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select
		case
			when start_day is not null
			then start_day
			else '2021-01-01'
		end as start_day_from into lastupdatetimestamp
	from (select max(date)-interval '1 days' start_day from analytic.waze_route_travel_times_agg) wazemax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;
    -- inserting data is incremental, since the last timestamp already in the table
	insert into analytic.waze_route_travel_times_agg
	SELECT ts.route_id,
    ts.year,
    ts.month,
    ts.day,
    ts.dow,
    ts.hour,
    ts.quarter,
    ts.hour + (ts.quarter::numeric / 60::numeric)::double precision AS hour_quarter,
    ((((((((ts.year || '-'::text) || ts.month) || '-'::text) || ts.day) || ' '::text) || ts.hour) || ':'::text) || ts.quarter)::timestamp without time zone AS date,
    avg(ts.travel_time)::integer AS travel_time
   FROM ( SELECT raw.route_id,
            raw.update_time,
            raw.travel_time,
            date_part('year'::text, raw.update_time) AS year,
            date_part('month'::text, raw.update_time) AS month,
            date_part('day'::text, raw.update_time) AS day,
            date_part('dow'::text, raw.update_time) AS dow,
            date_part('hour'::text, raw.update_time) AS hour,
            date_part('minute'::text, raw.update_time) AS minute,
            date_part('minute'::text, raw.update_time)::integer / 15 * 15 AS quarter
           FROM ( SELECT wazett_route_lives.route_id,
                    timezone('Europe/Prague'::text, to_timestamp((wazett_route_lives.update_time / 1000)::double precision)::timestamp without time zone) AS update_time,
                    wazett_route_lives."time" AS travel_time
                   FROM wazett_route_lives
                     JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = wazett_route_lives.route_id
			  where wazett_route_lives.update_time > lastupdateunix -- this is the time condition for the increment                 
                     ) raw
           ) ts
  WHERE ts.year > 2000::double precision
  GROUP BY ts.route_id, ts.year, ts.month, ts.day, ts.dow, ts.hour, ts.quarter
 	ON CONFLICT (route_id, year, month, day, dow, hour, quarter, hour_quarter)
		DO update
		SET 
	    date = EXCLUDED.date,
    	travel_time = EXCLUDED.travel_time
  ;
end;
$procedure$
;
