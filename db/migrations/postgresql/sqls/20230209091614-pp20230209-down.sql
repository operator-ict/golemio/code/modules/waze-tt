CREATE OR REPLACE PROCEDURE analytic.update_waze()
 LANGUAGE plpgsql
AS $procedure$

declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select
		case
			when start_day is not null
			then start_day
			else '2021-01-01'
		end as start_day_from into lastupdatetimestamp
	from (select max(date)-interval '1 days' start_day from analytic.waze_route_travel_times_agg) wazemax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;
    -- inserting data is incremental, since the last timestamp already in the table
	insert into analytic.waze_route_travel_times_agg
	SELECT ts.route_id,
    ts.year,
    ts.month,
    ts.day,
    ts.dow,
    ts.hour,
    ts.quarter,
    ts.hour + (ts.quarter::numeric / 60::numeric)::double precision AS hour_quarter,
    ((((((((ts.year || '-'::text) || ts.month) || '-'::text) || ts.day) || ' '::text) || ts.hour) || ':'::text) || ts.quarter)::timestamp without time zone AS date,
    avg(ts.travel_time)::integer AS travel_time
   FROM ( SELECT raw.route_id,
            raw.update_time,
            raw.travel_time,
            date_part('year'::text, raw.update_time) AS year,
            date_part('month'::text, raw.update_time) AS month,
            date_part('day'::text, raw.update_time) AS day,
            date_part('dow'::text, raw.update_time) AS dow,
            date_part('hour'::text, raw.update_time) AS hour,
            date_part('minute'::text, raw.update_time) AS minute,
            date_part('minute'::text, raw.update_time)::integer / 15 * 15 AS quarter
           FROM ( SELECT wazett_route_lives.route_id,
                    timezone('Europe/Prague'::text, to_timestamp((wazett_route_lives.update_time / 1000)::double precision)::timestamp without time zone) AS update_time,
                    wazett_route_lives."time" AS travel_time
                   FROM wazett_route_lives
                     JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = wazett_route_lives.route_id
			  where wazett_route_lives.update_time > lastupdateunix -- this is the time condition for the increment                 
                     ) raw
           ) ts
  WHERE ts.year > 2000::double precision
  GROUP BY ts.route_id, ts.year, ts.month, ts.day, ts.dow, ts.hour, ts.quarter
 	ON CONFLICT (route_id, year, month, day, dow, hour, quarter, hour_quarter)
		DO update
		SET 
	    date = EXCLUDED.date,
    	travel_time = EXCLUDED.travel_time
  ;
end;
$procedure$
;


CREATE OR REPLACE PROCEDURE analytic.update_mala_strana()
 LANGUAGE plpgsql
AS $procedure$
declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	begin
	select
		case
			when start_day is not null
			then start_day
			else '2022-01-01'
		end as start_day_from into lastupdatetimestamp
	from (select max(datum)-interval '1 days' start_day from analytic.mala_strana_route_hour_core) barandemax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;

-- dopoctu hodinovou agregaci (podobné v_mala_strana_route_hour_core) POT5EBUJEME TAKTO?
	insert into analytic.mala_strana_route_hour_core
		SELECT 
			wdrn.name,
		    rl.ts::date AS datum,
		    date_part('hour'::text, rl.ts) AS hodina,
		    (to_char(date_part('hour'::text, rl.ts), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, rl.ts) + 1::double precision, '09'::text) AS cas_interval,
		    avg_time,
		    avg_speed
		   FROM (
		     select
		     	route_id
--		     	timezone('Europe/Prague'::text, to_timestamp((update_time / 1000)::double precision)::timestamp without time zone)
		   		,date_trunc('hour', to_timestamp((update_time / 1000))::timestamp) as ts
		   		,round(avg(time), 2) AS avg_time
		   		,round(avg(rl.length * 1000 / (nullif(rl."time", 0) * 360)), 2) AS avg_speed
		   	from wazett.wazett_route_lives rl
		   	where
--		   		route_id = 22706 and
				rl.update_time > lastupdateunix and
				rl.length > 0
		   	group by 1,2) rl
		     JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = rl.route_id
		ON CONFLICT (name,datum,hodina,cas_interval)
			DO update
			SET avg_time = EXCLUDED.avg_time,
				avg_speed = EXCLUDED.avg_speed
;
		
	insert into	analytic.mala_strana_data_all
	WITH
		v_barrande_route_hour as (
	SELECT p.name,
    p.datum,
    p.hodina,
    p.cas_interval,
    p.avg_time,
    p.avg_speed,
    n.avg_time AS normal_avg_time,
    n.avg_speed AS normal_avg_speed
   FROM analytic.mala_strana_route_hour_core p
     LEFT JOIN analytic.mv_mala_strana_normal n ON n.name = p.name AND p.hodina = n.hodina
	where datum > lastupdatetimestamp
		),
-- konec v_barrande_route_hour
		v_barrande_union_time_interval as (
		WITH hodiny AS (
         SELECT brh.name,
            brh.datum,
            brh.hodina,
            brh.cas_interval,
            brh.avg_time,
            brh.avg_speed,
            brh.normal_avg_time,
            brh.normal_avg_speed
           FROM v_barrande_route_hour brh
          WHERE brh.datum < CURRENT_DATE OR brh.hodina < date_part('hour'::text, now())
        ), den AS (
         SELECT hodiny.name,
            hodiny.datum,
            '0 - 24h'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h15_18 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '15 -18'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 15::double precision AND hodiny.hodina <= 17::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 17::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h22_6 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '22 - 06'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE NOT (hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision) AND hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.name, hodiny.datum
        ), h6_22 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '06 - 22'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 21::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_10 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 10'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 7::double precision AND hodiny.hodina <= 9::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 10::double precision)
          GROUP BY hodiny.name, hodiny.datum
        ), h7_11_15_19 AS (
         SELECT hodiny.name,
            hodiny.datum,
            '07 - 11, 15 - 19'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE (hodiny.hodina >= 7::double precision AND hodiny.hodina <= 10::double precision OR hodiny.hodina >= 15::double precision AND hodiny.hodina <= 18::double precision) AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 18::double precision)
          GROUP BY hodiny.name, hodiny.datum
        )
 SELECT hodiny.name,
    hodiny.datum,
    hodiny.cas_interval,
    hodiny.avg_time,
    hodiny.avg_speed,
    hodiny.normal_avg_time,
    hodiny.normal_avg_speed
   FROM hodiny
UNION ALL
 SELECT den.name,
    den.datum,
    den.cas_interval,
    den.avg_time,
    den.avg_speed,
    den.normal_avg_time,
    den.normal_avg_speed
   FROM den
UNION ALL
 SELECT h15_18.name,
    h15_18.datum,
    h15_18.cas_interval,
    h15_18.avg_time,
    h15_18.avg_speed,
    h15_18.normal_avg_time,
    h15_18.normal_avg_speed
   FROM h15_18
UNION ALL
 SELECT h22_6.name,
    h22_6.datum,
    h22_6.cas_interval,
    h22_6.avg_time,
    h22_6.avg_speed,
    h22_6.normal_avg_time,
    h22_6.normal_avg_speed
   FROM h22_6
UNION ALL
 SELECT h6_22.name,
    h6_22.datum,
    h6_22.cas_interval,
    h6_22.avg_time,
    h6_22.avg_speed,
    h6_22.normal_avg_time,
    h6_22.normal_avg_speed
   FROM h6_22
UNION ALL
 SELECT h7_10.name,
    h7_10.datum,
    h7_10.cas_interval,
    h7_10.avg_time,
    h7_10.avg_speed,
    h7_10.normal_avg_time,
    h7_10.normal_avg_speed
   FROM h7_10
UNION ALL
 SELECT h7_11_15_19.name,
    h7_11_15_19.datum,
    h7_11_15_19.cas_interval,
    h7_11_15_19.avg_time,
    h7_11_15_19.avg_speed,
    h7_11_15_19.normal_avg_time,
    h7_11_15_19.normal_avg_speed
   FROM h7_11_15_19
		),
-----------------------
		data1 AS (
        SELECT
			trasy.name,
            trasy.name_trasa,
            trasy.name_usek,
			CASE
				WHEN trasy.is_opacny_smer THEN 'Zpět'::text
				ELSE 'Tam'::text
			END AS smer,
            vbu.datum,
            vbu.cas_interval,
            vbu.avg_time,
            vbu.avg_speed,
            vbu.normal_avg_time,
            vbu.normal_avg_speed
        FROM v_barrande_union_time_interval vbu
            JOIN pavelp.v_mala_strana_route trasy ON trasy.name = vbu.name
        ),
		data2 AS (
        SELECT
			data1.name_trasa,
            data1.name_usek,
            'Oba'::text AS smer,
            data1.datum,
            data1.cas_interval,
            sum(data1.avg_time) AS avg_time,
            avg(data1.avg_speed) AS avg_speed,
            sum(data1.normal_avg_time) AS normal_avg_time,
            avg(data1.normal_avg_speed) AS normal_avg_speed
        FROM data1
        GROUP BY data1.name_trasa, data1.name_usek, 'Oba'::text, data1.datum, data1.cas_interval
        )
 SELECT data1.name,
    data1.name_trasa,
    data1.name_usek,
    data1.smer,
    data1.datum,
    data1.cas_interval,
    data1.avg_time,
    data1.avg_speed,
    data1.normal_avg_time,
    data1.normal_avg_speed
   FROM data1
UNION
 SELECT vbt.name,
    data2.name_trasa,
    data2.name_usek,
    data2.smer,
    data2.datum,
    data2.cas_interval,
    data2.avg_time,
    data2.avg_speed,
    data2.normal_avg_time,
    data2.normal_avg_speed
   FROM data2
     JOIN analytic.v_mala_strana_route vbt ON vbt.name_trasa = data2.name_trasa AND vbt.name_usek = data2.name_usek AND NOT vbt.is_opacny_smer
	on conflict (name,name_trasa,name_usek,smer,datum,cas_interval)
	do update
		SET avg_time = EXCLUDED.avg_time,
		avg_speed = EXCLUDED.avg_speed,
		normal_avg_time = EXCLUDED.normal_avg_time,
		normal_avg_speed = EXCLUDED.normal_avg_speed;
	end;
end;
$procedure$
;

-- analytic.mv_mala_strana_normal source
DROP MATERIALIZED VIEW analytic.mv_mala_strana_normal;
CREATE MATERIALIZED VIEW analytic.mv_mala_strana_normal
TABLESPACE pg_default
AS SELECT vtbh.name,
    vtbh.hodina,
    round(avg(vtbh.avg_time), 2) AS avg_time,
    round(avg(vtbh.avg_speed)) AS avg_speed
   FROM ( SELECT wdrn.name,
                CASE
                    WHEN wdrn.idx < 38 THEN 'group1'::text
                    ELSE 'group2'::text
                END AS skupina,
            rl.ts::date AS datum,
            date_part('hour'::text, rl.ts) AS hodina,
            (to_char(date_part('hour'::text, rl.ts), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, rl.ts) + 1::double precision, '09'::text) AS cas_interval,
            rl.avg_time,
            rl.avg_speed
           FROM ( SELECT rl_1.route_id,
                    date_trunc('hour'::text, to_timestamp((rl_1.update_time / 1000)::double precision)) AS ts,
                    round(avg(rl_1."time"), 2) AS avg_time,
                    round(avg(rl_1.length * 1000 / (NULLIF(rl_1."time", 0) * 360)), 2) AS avg_speed
                   FROM wazett.wazett_route_lives rl_1
                  WHERE rl_1.update_time >= '1665360000000'::bigint
                  GROUP BY rl_1.route_id, (date_trunc('hour'::text, to_timestamp((rl_1.update_time / 1000)::double precision)))) rl
             JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = rl.route_id) vtbh
  WHERE vtbh.skupina = 'group1'::text AND vtbh.datum >= '2022-10-10'::date AND vtbh.datum <= '2022-10-25'::date AND (date_part('dow'::text, vtbh.datum) = ANY (ARRAY[2::double precision, 3::double precision, 4::double precision])) OR vtbh.skupina = 'group1'::text AND vtbh.datum >= '2023-01-09'::date AND vtbh.datum <= '2023-01-29'::date AND (date_part('dow'::text, vtbh.datum) = ANY (ARRAY[2::double precision, 3::double precision, 4::double precision]))
  GROUP BY vtbh.name, vtbh.hodina
WITH DATA;