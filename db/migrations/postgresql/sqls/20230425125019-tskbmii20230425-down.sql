drop PROCEDURE analytic.update_tsk_bm2;

drop TABLE analytic.tsk_bm2_route_hour_core;
drop TABLE analytic.tsk_bm2_data_all;

drop MATERIALIZED VIEW analytic.mv_tsk_bm2_normal;
drop VIEW analytic.v_tsk_bm2_route;