CREATE OR REPLACE view analytic.v_sck_last_update as
select to_char(to_timestamp(update_time/1000),'dd.MM.yyyy hh24:mi:ss') last_update  
from analytic.v_sck_route_live vsrl 
order by update_time desc 
limit 1;
