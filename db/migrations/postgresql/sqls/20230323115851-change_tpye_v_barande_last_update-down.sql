DROP VIEW analytic.v_barrande_last_update;

CREATE OR REPLACE VIEW analytic.v_barrande_last_update
AS SELECT to_timestamp((wrl.update_time / 1000)::double precision) AS last_update
   FROM wazett.wazett_route_lives wrl
     LEFT JOIN wazett.wazett_routes wr ON wrl.route_id = wr.id
  WHERE "left"(wr.name, 3) = 'BM '::text
  ORDER BY wrl.update_time DESC
 LIMIT 1;
