-- wazett_routes
alter table wazett_routes alter column last_update type timestamptz using to_timestamp(last_update / 1000.00);


-- wazett_jams_stats
alter table wazett_jams_stats rename to wazett_jams_stats_old;

CREATE TABLE wazett_jams_stats (
	feed_id int4 NOT NULL,
	update_time timestamptz NOT NULL,
	jam_level int4 NOT NULL,
	wazers_count numeric NULL,
	length_of_jams int4 NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_jams_stats_pkey_new PRIMARY KEY (feed_id, update_time, jam_level),
	CONSTRAINT wazett_jams_stats_to_feed_id_new FOREIGN KEY (feed_id) REFERENCES wazett_feeds(id)
);

alter table wazett_jams_stats_old drop constraint wazett_jams_stats_to_feed_id;


-- wazett_route_lives
alter table wazett_route_lives rename to wazett_route_lives_old;

CREATE TABLE wazett_route_lives (
	route_id int8 NOT NULL,
	update_time timestamptz NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_route_lives_pkey_new PRIMARY KEY (update_time, route_id),
	CONSTRAINT wazett_route_lives_to_route_id_new FOREIGN KEY (route_id) REFERENCES wazett_routes(id)
);

alter table wazett_route_lives_old drop constraint wazett_route_lives_to_route_id;


-- wazett_subroute_lives
alter table wazett_subroute_lives rename to wazett_subroute_lives_old;

CREATE TABLE wazett_subroute_lives (
	route_id int8 NOT NULL,
	subroute_line_md5 uuid NOT NULL,
	update_time timestamptz NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_subroute_lives_pk_new PRIMARY KEY (route_id, update_time, subroute_line_md5),
	CONSTRAINT wazett_subroute_lives_fk_new FOREIGN KEY (subroute_line_md5,route_id) REFERENCES wazett_subroutes(line_md5,route_id),
	CONSTRAINT wazett_subroute_lives_to_route_id_new FOREIGN KEY (route_id) REFERENCES wazett_routes(id)
);

alter table wazett_subroute_lives_old drop constraint wazett_subroute_lives_fk;
alter table wazett_subroute_lives_old drop constraint wazett_subroute_lives_to_route_id;
