-- analytic.lkpr_dashboard_route_names definition

CREATE TABLE analytic.lkpr_dashboard_route_names (
	route_type text NULL,
	route_num text NULL,
	route_group_name text NULL,
	order_idx int4 NULL
);

-- analytic.v_instatntomtom_days source

CREATE OR REPLACE VIEW analytic.v_instatntomtom_days
AS SELECT to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text) AS day_index,
    wrl.route_id,
    round(avg(wrl."time"::numeric / wrl.historic_time::numeric), 2) AS t_index_value,
    count(*) AS t_index_count
   FROM wazett.wazett_route_lives wrl
  WHERE to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone >= '05:00:00'::time without time zone AND to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone <= '21:00:00'::time without time zone
  GROUP BY (to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text)), wrl.route_id;


-- analytic.v_lkpr_route_details source

CREATE OR REPLACE VIEW analytic.v_lkpr_route_details
AS SELECT re.id,
    re.name,
    re.route_code,
    re.route_name,
    re.from_name,
    re.to_name,
    re.route_type,
    re.route_num,
    re.route_variant,
        CASE
            WHEN re.route_variant = 'A1'::text THEN 'Na letiště'::text
            WHEN re.route_variant = 'B1'::text THEN 'Z letiště'::text
            ELSE 'Ostatní'::text
        END AS direction,
    drn.route_group_name,
    drn.order_idx
   FROM ( SELECT rc.id,
            rc.name,
            rc.route_code,
            rc.route_name,
            rc.from_name,
            rc.to_name,
            split_part(rc.route_code, '-'::text, 1) AS route_type,
            split_part(rc.route_code, '-'::text, 2) AS route_num,
            split_part(rc.route_code, '-'::text, 3) AS route_variant
           FROM ( SELECT r.id,
                    r.name,
                    split_part(r.name, ' '::text, 1) AS route_code,
                    "substring"(r.name, "position"(r.name, ' '::text) + 1) AS route_name,
                    r.from_name,
                    r.to_name
                   FROM wazett.wazett_routes r
                  WHERE r.feed_id = 1) rc) re
     LEFT JOIN analytic.lkpr_dashboard_route_names drn ON re.route_type = drn.route_type AND re.route_num = drn.route_num
  ORDER BY drn.order_idx;

-- public.v_lkpr_export source

CREATE OR REPLACE VIEW public.v_lkpr_export
AS SELECT COALESCE(vrd.id, wrl.route_id) AS route_id,
    vrd.name,
    vrd.route_name,
    vrd.from_name,
    vrd.to_name,
    vrd.route_num,
    vrd.direction,
    vrd.order_idx,
    to_timestamp((wrl.update_time / 1000)::double precision) AS update_time,
    wrl."time" AS actual_time,
    round(wrl.length::numeric * 3.6 / wrl."time"::numeric) AS actual_speed,
    wrl.historic_time,
    round(wrl.length::numeric * 3.6 / wrl.historic_time::numeric) AS historic_speed,
    wrl.length
   FROM analytic.v_lkpr_route_details vrd
     FULL JOIN wazett.wazett_route_lives wrl ON vrd.id = wrl.route_id
  WHERE vrd.name ~~ 'TN-%'::text OR vrd.route_code ~~ 'TN-%'::text;

-- analytic.v_sck_route_details source

CREATE OR REPLACE VIEW analytic.v_sck_route_details
AS SELECT DISTINCT ON ((wazett_routes.name::json ->> 'project'::text), (wazett_routes.name::json ->> 'route number'::text), (wazett_routes.name::json ->> 'dir'::text)) wazett_routes.id,
    concat(wazett_routes.name::json ->> 'route number'::text, ' ', wazett_routes.name::json ->> 'name'::text) AS name,
    false AS section,
        CASE
            WHEN (wazett_routes.name::json ->> 'dir'::text) = 'Tam'::text THEN false
            ELSE true
        END AS reverse_direction,
    wazett_routes.from_name,
    wazett_routes.to_name,
    wazett_routes.name::json ->> 'route number'::text AS poradi
   FROM wazett.wazett_routes
  WHERE wazett_routes.feed_id = 7 AND wazett_routes.name ~~ '{%'::text AND (wazett_routes.name::json ->> 'project'::text) = 'IPR SčK'::text
  ORDER BY (wazett_routes.name::json ->> 'project'::text), (wazett_routes.name::json ->> 'route number'::text), (wazett_routes.name::json ->> 'dir'::text), wazett_routes.created_at DESC;


-- analytic.v_sck_route_live source

CREATE OR REPLACE VIEW analytic.v_sck_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett.wazett_route_lives wrl
  WHERE (wrl.route_id IN ( SELECT DISTINCT vsrd.id
           FROM analytic.v_sck_route_details vsrd)) AND wrl.update_time >= 1675206000;


-- analytic.v_sck_last_update source

CREATE OR REPLACE VIEW analytic.v_sck_last_update
AS SELECT to_char(to_timestamp((vsrl.update_time / 1000)::double precision), 'dd.MM.yyyy hh24:mi:ss'::text) AS last_update
   FROM analytic.v_sck_route_live vsrl
  ORDER BY vsrl.update_time DESC
 LIMIT 1;



-- analytic.v_wazett_routes_lines source

CREATE OR REPLACE VIEW analytic.v_wazett_routes_lines
AS SELECT wazett_routes.id,
    wazett_routes.name,
    st_astext(wazett_routes.line) AS line,
    concat('#', "left"(lpad(to_hex((wazett_routes.id::double precision * character_length(wazett_routes.name)::double precision / (( SELECT min(wazett_routes_1.id * character_length(wazett_routes_1.name)) AS min
           FROM wazett.wazett_routes wazett_routes_1))::double precision * 10000000::double precision)::bigint), 6, '0'::text), 6)) AS color
   FROM wazett.wazett_routes;