-- pavelp.v_sck_route_details source

CREATE OR REPLACE VIEW analytic.v_sck_route_details
AS
SELECT DISTINCT
        ON ((wazett_routes.name::json ->> 'project'::text), (wazett_routes.name::json ->> 'route number'::text), (wazett_routes.name::json ->> 'dir'::text)) wazett_routes.id,
        wazett_routes.name::json ->> 'name'::text AS name                                                                                                               ,
        false                                          AS section                                                                                                            ,
        CASE
        WHEN
                (wazett_routes.name::json ->> 'dir'::text) = 'Tam'::text
        THEN
                false
        ELSE
                true
        END AS reverse_direction,
        wazett_routes.from_name ,
        wazett_routes.to_name   ,
        wazett_routes.name::json ->> 'route number'::text AS poradi
FROM
        wazett.wazett_routes
WHERE
        wazett_routes.feed_id = 7
AND     wazett_routes.name ~~ '{%'::text
AND     (
                wazett_routes.name::json ->> 'project'::text) = 'IPR SčK'::text
ORDER BY
        (wazett_routes.name::json ->> 'project'::text)     ,
        (wazett_routes.name::json ->> 'route number'::text),
        (wazett_routes.name::json ->> 'dir'::text)         ,
        wazett_routes.created_at DESC;

