-- pavelp.v_sck_route_details source

CREATE OR REPLACE VIEW analytic.v_sck_route_details
AS
SELECT DISTINCT
        ON ((wazett_routes.name::json ->> 'project'::text), (wazett_routes.name::json ->> 'route number'::text), (wazett_routes.name::json ->> 'dir'::text)) wazett_routes.id,
        wazett_routes.name::json ->> 'name'::text AS name                                                                                                               ,
        false                                          AS section                                                                                                            ,
        CASE
        WHEN
                (wazett_routes.name::json ->> 'dir'::text) = 'Tam'::text
        THEN
                false
        ELSE
                true
        END AS reverse_direction,
        wazett_routes.from_name ,
        wazett_routes.to_name   ,
        wazett_routes.name::json ->> 'route number'::text AS poradi
FROM
        wazett.wazett_routes
WHERE
        wazett_routes.feed_id = 7
AND     wazett_routes.name ~~ '{%'::text
AND     (
                wazett_routes.name::json ->> 'project'::text) = 'IPR SčK'::text
ORDER BY
        (wazett_routes.name::json ->> 'project'::text)     ,
        (wazett_routes.name::json ->> 'route number'::text),
        (wazett_routes.name::json ->> 'dir'::text)         ,
        wazett_routes.created_at DESC;


		
		
-- v_sck_route_live source

CREATE OR REPLACE VIEW analytic.v_sck_route_live
AS
SELECT
        wrl.route_id       ,
        wrl.update_time    ,
        wrl."time"         ,
        wrl.length         ,
        wrl.historic_time  ,
        wrl.jam_level      ,
        wrl.create_batch_id,
        wrl.created_at     ,
        wrl.created_by     ,
        wrl.update_batch_id,
        wrl.updated_at     ,
        wrl.updated_by
FROM
        wazett.wazett_route_lives wrl
WHERE
        (
                wrl.route_id IN
                (
                        SELECT DISTINCT
                                vsrd.id
                        FROM
                                analytic.v_sck_route_details vsrd))
AND     wrl.update_time::double precision >= (date_part('epoch'::text, CURRENT_DATE - '7 days'::interval) * 1000::double precision);