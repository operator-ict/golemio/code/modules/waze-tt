drop PROCEDURE analytic.update_waze;
drop TABLE analytic.waze_route_travel_times_agg;

drop PROCEDURE analytic.update_tsk_bm2;
drop MATERIALIZED VIEW analytic.mv_tsk_bm2_normal;
drop VIEW analytic.v_tsk_bm2_route;

drop TABLE analytic.tsk_bm2_data_all;
drop TABLE analytic.tsk_bm2_route_hour_core;

drop PROCEDURE analytic.update_mala_strana;
drop MATERIALIZED VIEW analytic.mv_mala_strana_normal;
drop VIEW analytic.v_mala_strana_route;


drop VIEW analytic.v_route_travel_times;
drop VIEW analytic.v_route_live;

drop TABLE if exists analytic.mala_strana_data_all;
drop TABLE if exists analytic.mala_strana_route_hour_core;
drop TABLE if exists analytic.waze_dashboard_route_name;


drop VIEW analytic.v_barrande_data_all;
drop VIEW analytic.v_barrande_route;
drop VIEW analytic.v_barrande_last_update;


drop VIEW analytic.v_barrande_union_time_interval;
drop VIEW analytic.v_barrande_route_hour;

drop MATERIALIZED VIEW if exists analytic.mv_barrande_normal;
drop VIEW analytic.v_barrande_route_hour_core;


drop VIEW analytic.v_barrandov_bridge_route_details;
Drop VIEW analytic.v_barrandov_bridge_route_live;

drop PROCEDURE analytic.update_barande;

drop TABLE if exists analytic.barrande_data_all;
drop TABLE if exists analytic.barrande_route_hour_core ;
drop TABLE if exists analytic.barrande_black_list_route;
