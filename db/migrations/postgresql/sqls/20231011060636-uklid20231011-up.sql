drop view analytic.v_wazett_routes_lines;
drop view analytic.v_sck_last_update;
drop view analytic.v_sck_route_live;
drop view analytic.v_sck_route_details;

drop view public.v_lkpr_export;
drop view analytic.v_lkpr_route_details;
drop view analytic.v_instatntomtom_days;

DROP TABLE analytic.lkpr_dashboard_route_names;