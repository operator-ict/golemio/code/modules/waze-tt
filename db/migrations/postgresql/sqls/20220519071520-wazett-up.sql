-- Drop table

-- DROP TABLE wazett_feeds;

CREATE TABLE wazett_feeds (
	id int4 NOT NULL,
	area_name text NULL,
	broadcaster_id text NULL,
	"name" text NULL,
	bbox jsonb NULL,
	ismetric bool NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_feeds_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE wazett_jams_stats;

CREATE TABLE wazett_jams_stats (
	feed_id int4 NOT NULL,
	update_time int8 NOT NULL,
	jam_level int4 NOT NULL,
	wazers_count numeric NULL,
	length_of_jams int4 NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_jams_stats_pkey PRIMARY KEY (feed_id, update_time, jam_level),
	CONSTRAINT wazett_jams_stats_to_feed_id FOREIGN KEY (feed_id) REFERENCES wazett_feeds(id)
);

-- Drop table

-- DROP TABLE wazett_routes;

CREATE TABLE wazett_routes (
	id int8 NOT NULL,
	feed_id int4 NULL,
	from_name text NULL,
	to_name text NULL,
	"name" text NULL,
	bbox jsonb NULL,
	last_update int8 NULL,
	line geometry(linestring, 4326) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_routes_pkey PRIMARY KEY (id),
	CONSTRAINT wazett_routes_to_feed_id FOREIGN KEY (feed_id) REFERENCES wazett_feeds(id)
);

-- Drop table

-- DROP TABLE wazett_route_lives;

CREATE TABLE wazett_route_lives (
	route_id int8 NOT NULL,
	update_time int8 NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_route_lives_pkey PRIMARY KEY (update_time, route_id),
	CONSTRAINT wazett_route_lives_to_route_id FOREIGN KEY (route_id) REFERENCES wazett_routes(id)
);

-- Drop table

-- DROP TABLE wazett_subroutes;

CREATE TABLE wazett_subroutes (
    line_md5 uuid NOT NULL,
	route_id int8 NOT NULL,
	line geometry(linestring, 4326) NULL,
	from_name text NULL,
	to_name text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_subroutes_pk PRIMARY KEY (line_md5, route_id),
	CONSTRAINT wazett_subroutes_fk FOREIGN KEY (line_md5,route_id) REFERENCES wazett_subroutes(line_md5,route_id),
	CONSTRAINT wazett_subroutes_to_route_id FOREIGN KEY (route_id) REFERENCES wazett_routes(id)
);

-- Drop table

-- DROP TABLE wazett_subroute_lives;

CREATE TABLE wazett_subroute_lives (
	route_id int8 NOT NULL,
    subroute_line_md5 uuid NOT NULL,
	update_time int8 NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_subroute_lives_pk PRIMARY KEY (route_id, update_time, subroute_line_md5),
	CONSTRAINT wazett_subroute_lives_fk FOREIGN KEY (subroute_line_md5,route_id) REFERENCES wazett_subroutes(line_md5,route_id),
	CONSTRAINT wazett_subroute_lives_to_route_id FOREIGN KEY (route_id) REFERENCES wazett_routes(id)
);
