import { WazeTT } from "#sch/index";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import * as fs from "fs";
import "mocha";

chai.use(chaiAsPromised);

describe("WazeTT - Datasource Validation", () => {
    let testSourceData;
    let validator: JSONSchemaValidator;

    before(() => {
        validator = new JSONSchemaValidator(WazeTT.name + "DataSource", WazeTT.datasourceWazeTTJsonSchema);
    });

    it("is succefull", async () => {
        const testSourceDataStr = fs.readFileSync(__dirname + "/data/wazett-dataMockup20220513.json").toString("utf8");
        testSourceData = JSON.parse(testSourceDataStr);
        expect(await validator.Validate(testSourceData)).to.equal(true);
    });

    it("invalid data", async () => {
        const testSourceDataStr = fs.readFileSync(__dirname + "/data/wazett-dataMockup20220513_err.json").toString("utf8");
        testSourceData = JSON.parse(testSourceDataStr);
        await expect(validator.Validate(testSourceData)).to.be.rejectedWith(Error);
    });
});
