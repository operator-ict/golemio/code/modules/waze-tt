import {
    WazeTTTransformationsRouteLives,
    WazeTTTransformationsRoutes,
    WazeTTTransformationsSubRouteLives,
    WazeTTTransformationsSubRoutes,
} from "#ie";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { WazeTTDataService } from "#ie/services/WazeTTDataService";
import { WazeTT } from "#sch/index";
import { IWazeTTFeed } from "#sch/interfaces/WazeTTData";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import fs from "fs";
import "mocha";
import path from "path";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("WazeTTDataService", () => {
    let createContainer: () => WazeTTDataService;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;
    let testData: IWazeTTFeed;

    let testTransformedData: number[];
    let inputData: any;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        testData = {
            id: "5",
            sourceId: 1,
            usersOnJams: [
                { wazersCount: 0, jamLevel: 0 },
                { wazersCount: 0, jamLevel: 1 },
                { wazersCount: 0, jamLevel: 2 },
                { wazersCount: 0, jamLevel: 3 },
                { wazersCount: 0, jamLevel: 4 },
            ],
            lengthOfJams: [
                { jamLevel: 1, jamLength: 0 },
                { jamLevel: 2, jamLength: 0 },
                { jamLevel: 3, jamLength: 2545 },
                { jamLevel: 4, jamLength: 0 },
                { jamLevel: 5, jamLength: 7605 },
            ],
            updateTime: 1612931164533,
            areaName: "Prague",
            bbox: { minY: 50.009, minX: 14.178, maxY: 50.171, maxX: 14.463 },
            broadcasterId: "5ab8d05c000ebc0d14059c045bc715fe",
            isMetric: true,
            name: "LKPR",
            routes: [
                {
                    toName: "Aviatická",
                    historicTime: 1247,
                    line: [
                        { x: 14.409817827409382, y: 50.09123625550184 },
                        { x: 14.409615077104998, y: 50.09168137468799 },
                        { x: 14.409615077104998, y: 50.09168137468799 },
                    ],
                    bbox: {
                        minY: 50.07514017120593,
                        minX: 14.271311830618327,
                        maxY: 50.11627808466555,
                        maxX: 14.409817827409382,
                    },
                    name: "TN-09-A1 Klárov => Letiště",
                    fromName: "Klárov",
                    length: 16373,
                    jamLevel: 0,
                    id: "24924",
                    time: 1258,
                    type: "STATIC",
                    jams: [],
                    subRoutes: [
                        {
                            toName: "Aviatická",
                            historicTime: 1247,
                            line: [
                                { x: 14.409817827409382, y: 50.09123625550184 },
                                { x: 14.409615077104998, y: 50.09168137468799 },
                                { x: 14.409615077104998, y: 50.09168137468799 },
                            ],
                            bbox: {
                                minY: 50.07514017120593,
                                minX: 14.271311830618327,
                                maxY: 50.11627808466555,
                                maxX: 14.409817827409382,
                            },
                            name: "TN-09-A1 Klárov => Letiště",
                            fromName: "Klárov",
                            length: 16373,
                            jamLevel: 0,
                            id: "24924",
                            time: 1258,
                            type: "STATIC",
                            jams: [],
                        },
                    ],
                },
            ],
        };

        testTransformedData = [1, 2];

        inputData = JSON.parse(fs.readFileSync(path.join(__dirname, "data", "wazett-dataMockup20230418.json"), "utf8"));

        stubs = {};
        stubs.transformationFeedsTransform = sandbox.stub().resolves(testTransformedData);
        stubs.transformationRoutesTransform = sandbox.stub().resolves(testTransformedData);
        stubs.transformationRouteLivesTransform = sandbox.stub().resolves(testTransformedData);
        stubs.transformationSubRoutesTransform = sandbox.stub().resolves(testTransformedData);
        stubs.transformationSubRouteLivesTransform = sandbox.stub().resolves(testTransformedData);

        stubs.loggerError = sandbox.stub();
        stubs.modelFeedsSave = sandbox.stub().resolves(testData);
        stubs.modelJamsStatsSave = sandbox.stub().resolves(testData);
        stubs.modelRouteLivesSave = sandbox.stub().resolves(testData);
        stubs.modelRoutesSave = sandbox.stub().resolves(testData);
        stubs.modelSubRouteLivesSave = sandbox.stub().resolves(testData);
        stubs.modelSubRoutesSave = sandbox.stub().resolves(testData);

        createContainer = () =>
            container
                .createChildContainer()
                .registerSingleton(CoreToken.Logger, class Logger {})
                // transformations
                .registerSingleton(
                    ModuleContainerToken.WazeTTTransformationsFeed,
                    class WazeTTTransformationsFeed {
                        transform = stubs.transformationFeedsTransform;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.WazeTTTransformationsSubRoutes,
                    class WazeTTTransformationsSubRoutes {
                        transform = stubs.transformationSubRoutesTransform;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.WazeTTTransformationsRoutes,
                    class WazeTTTransformationsRoutes {
                        transform = stubs.transformationRoutesTransform;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.WazeTTTransformationsSubRouteLives,
                    class WazeTTTransformationsSubRouteLives {
                        transform = stubs.transformationSubRouteLivesTransform;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.WazeTTTransformationsRouteLives,
                    class WazeTTTransformationsRouteLives {
                        transform = stubs.transformationRouteLivesTransform;
                    }
                )

                // repositories
                .registerSingleton(
                    ModuleContainerToken.FeedsRepository,
                    class FeedsRepository {
                        save = stubs.modelFeedsSave;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.JamsStatsRepository,
                    class JamsStatsRepository {
                        save = stubs.modelJamsStatsSave;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.RouteLivesRepository,
                    class RouteLivesRepository {
                        save = stubs.modelRouteLivesSave;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.RoutesRepository,
                    class RoutesRepository {
                        save = stubs.modelRoutesSave;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.SubRouteLivesRepository,
                    class SubRouteLivesRepository {
                        save = stubs.modelSubRouteLivesSave;
                    }
                )
                .registerSingleton(
                    ModuleContainerToken.SubRoutesRepository,
                    class SubRoutesRepository {
                        save = stubs.modelSubRoutesSave;
                    }
                )
                .register(ModuleContainerToken.WazeTTDataService, WazeTTDataService)
                .resolve<WazeTTDataService>(ModuleContainerToken.WazeTTDataService);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    it("should call the correct methods by saveFromSingleResource method", async () => {
        const service = createContainer();
        await service.saveFromSingleResource(testData);

        sandbox.assert.calledOnce(stubs.transformationFeedsTransform);
        sandbox.assert.calledWith(stubs.transformationFeedsTransform, testData);

        sandbox.assert.calledOnce(stubs.modelFeedsSave);
        sandbox.assert.called(stubs.modelJamsStatsSave);
        sandbox.assert.called(stubs.modelRoutesSave);
        sandbox.assert.called(stubs.modelRouteLivesSave);

        sandbox.assert.called(stubs.transformationRoutesTransform);
        sandbox.assert.called(stubs.transformationRouteLivesTransform);

        sandbox.assert.callOrder(stubs.transformationFeedsTransform, stubs.modelFeedsSave);
    });

    it("validate transformed route data", async () => {
        const routeTransformer = new WazeTTTransformationsRoutes();
        const routeValidator = new JSONSchemaValidator(
            WazeTT.routes.name + "ModelJSONSchemaValidator",
            WazeTT.routes.outputJsonSchemaDefinition
        );

        for (const route of inputData.routes) {
            const transformedRoute = await routeTransformer.transform(route);
            transformedRoute.feed_id = 0;
            transformedRoute.last_update = new Date(123456789);
            const result = await routeValidator.Validate(transformedRoute);
            expect(result).to.be.true;
        }
    });

    it("validate transformed route lives data", async () => {
        const routeTransformer = new WazeTTTransformationsRouteLives();
        const routeValidator = new JSONSchemaValidator(
            WazeTT.routeLives.name + "ModelJSONSchemaValidator",
            WazeTT.routeLives.outputJsonSchemaDefinition
        );

        for (const route of inputData.routes) {
            const transformedRoute = await routeTransformer.transform(route);
            transformedRoute.route_id = "123";
            transformedRoute.update_time = new Date(123456789);
            const result = await routeValidator.Validate(transformedRoute);
            expect(result).to.be.true;
        }
    });

    it("validate transformed subroute and subroutelives data", async () => {
        const subrouteTransformer = new WazeTTTransformationsSubRoutes();
        const subrouteLivesTransformer = new WazeTTTransformationsSubRouteLives();

        const subrouteValidator = new JSONSchemaValidator(
            WazeTT.subRoutes.name + "ModelJSONSchemaValidator",
            WazeTT.subRoutes.outputJsonSchemaDefinition
        );
        const subrouteLivesValidator = new JSONSchemaValidator(
            WazeTT.subRouteLives.name + "ModelJSONSchemaValidator",
            WazeTT.subRouteLives.outputJsonSchemaDefinition
        );

        for (const route of inputData.routes) {
            if (route.hasOwnProperty("subRoutes")) {
                for (const subroute of route.subRoutes) {
                    const transformedSubRoute = await subrouteTransformer.transform(subroute);
                    transformedSubRoute!.route_id = "123";

                    const result = await subrouteValidator.Validate(transformedSubRoute);
                    expect(result).to.be.true;

                    const transformedSubRouteLive = await subrouteLivesTransformer.transform(subroute);
                    transformedSubRouteLive.update_time = new Date(123456789);
                    transformedSubRouteLive.route_id = "123";
                    transformedSubRouteLive.subroute_line_md5 = transformedSubRoute!.line_md5;
                    const result2 = await subrouteLivesValidator.Validate(transformedSubRouteLive);
                    expect(result2).to.be.true;
                }
            }
        }
    });
});
