import SubrouteIdHelper from "#ie/wazeTTTransformations/helpers/SubRouteIdHelper";
import { expect } from "chai";
import "mocha";

describe("SubRouteIdHelper", () => {
    it("generate correct uuid", () => {
        // eslint-disable-next-line max-len
        //LINESTRING (14.409535 50.033164, 14.409414 50.033261, 14.409053 50.033614, 14.408812 50.033907, 14.408511 50.034388, 14.408345 50.034798, 14.40824 50.035292, 14.408219 50.035669, 14.408306 50.036606, 14.408318 50.036742, 14.408276 50.037374, 14.408223 50.037607, 14.408149 50.037819, 14.407837 50.038365)
        const mockSubroute = {
            line: {
                coordinates: [
                    [14.409535, 50.033164],
                    [14.409414, 50.033261],
                    [14.409053, 50.033614],
                    [14.408812, 50.033907],
                    [14.408511, 50.034388],
                    [14.408345, 50.034798],
                    [14.40824, 50.035292],
                    [14.408219, 50.035669],
                    [14.408306, 50.036606],
                    [14.408318, 50.036742],
                    [14.408276, 50.037374],
                    [14.408223, 50.037607],
                    [14.408149, 50.037819],
                    [14.407837, 50.038365],
                ],
                type: "LineString",
            },
        };

        expect(SubrouteIdHelper.GenerateLineUuid(mockSubroute as any)).eq("7b456370-e0cc-48f9-6a19-8bcf649ca766");
    });

    it("invalid input", () => {
        expect(() => SubrouteIdHelper.GenerateLineUuid({} as any)).to.throw(Error);
        expect(() => SubrouteIdHelper.GenerateLineUuid({ line: {} } as any)).to.throw(Error);
        expect(() => SubrouteIdHelper.GenerateLineUuid({ line: { type: "LineString" } } as any)).to.throw(Error);
        expect(() => SubrouteIdHelper.GenerateLineUuid({ line: { type: "x", coordinates: [] } } as any)).to.throw(Error);
        expect(() => SubrouteIdHelper.GenerateLineUuid({ line: { type: "x", coordinates: [[1, 1]] } } as any)).to.throw(Error);
    });
});
