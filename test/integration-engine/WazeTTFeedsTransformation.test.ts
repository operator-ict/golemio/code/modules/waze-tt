import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import * as fs from "fs";
import "mocha";

import { WazeTTTransformationsFeed } from "#ie/wazeTTTransformations";
import { WazeTT } from "#sch/index";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

chai.use(chaiAsPromised);

describe("WazeTTFeedsTransformation", () => {
    let transformation;
    let testSourceData;
    let validator;

    before(() => {
        validator = new JSONSchemaValidator(WazeTT.feeds.name + "ModelValidator", WazeTT.feeds.outputJsonSchemaDefinition);
    });

    beforeEach(() => {
        transformation = new WazeTTTransformationsFeed();

        const testSourceDataStr = fs.readFileSync(__dirname + "/data/wazett-datasource.json").toString("utf8");
        testSourceData = JSON.parse(testSourceDataStr);
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("WazeTT");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform the feed", async () => {
        const data = await transformation.transform(testSourceData);
        await expect(validator.Validate(data)).to.be.fulfilled;
        expect(data).to.have.property("area_name");
        expect(data).to.have.property("name");
        expect(data).to.have.property("broadcaster_id");
        expect(data).to.have.property("bbox");
        expect(data).to.have.property("ismetric");
    });
});
