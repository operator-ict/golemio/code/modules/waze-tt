# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.0] - 2025-02-24

### Changed

-   Rework worker to use AbstractWorker class ([waze-tt#15](https://gitlab.com/operator-ict/golemio/code/modules/waze-tt/-/issues/15))
-   Add required values to JSON schema for data source

## [1.2.6] - 2024-10-16

### Added

-   AsyncAPI documentation ([integration-engine#262](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/262))

## [1.2.5] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.4] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.3] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.2.2] - 2024-02-21

### Removed

    - tables: wazett_subroute_lives_old, wazett_route_lives_old, wazett_jams_stats_old

## [1.2.1] - 2023-10-30

### Changed

-   Replace bigint timestamps ([waze-tt#13](https://gitlab.com/operator-ict/golemio/code/modules/waze-tt/-/issues/13))

## [1.2.0] - 2023-10-18

### Removed

-   drop views and table from schema analytic

## [1.1.10] - 2023-09-04

### Removed

-   drop analytic objets (tables, views, materialized views, procedures). Change to transforms (dbt)

## [1.1.9] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.8] - 2023-05-10

### Added

-   new views and tables for https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/46

## [1.1.7] - 2023-04-26

## Changed

-   mongo validator changed to json schema validator ([core#66](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/66))

## [1.1.6] - 2023-04-05

### Added

-   new view for https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/42

## [1.1.5] - 2023-04-03

### Changed

-   modified view v_barrande_last_update (retype datetime)
-   changed view v_barrandov_bridge_route_live - modified column update_time with unix time to timestamptz

## [1.1.4] - 2023-03-29

### Added

-   new views for https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/42

### Changed

-   fix view analytic.v_sck_route_details
-   modified view v_barrande_last_update (retype datetime)
-   fix view analytic.v_sck_route_details analytic.v_sck_route_details

## [1.1.3] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.2] - 2023-02-15

### Changed

-   Modification view v_wazett_routes_lines (https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/37)
-   Analytics, Wazett : new tables analytic.mala_strana_route_hour_core, analytic.mala_strana_data_all, new view analytic.v_mala_strana_route, new materialized view analytic.mv_mala_strana_normal, procedure analytic.update_mala_strana
-   Analytic, wazett: oprava dvou procedur analytic.update_waze(), analytic.update_mala_strana()

## [1.1.1] - 2023-02-08

### Changed

-   Changed data type from int to bigint in table analytic.waze_dashboard_route_name

### Added

-   Analytics, Wazett: new analytic table waze_route_travel_times_agg and procedure to update the table with pre-aggregated waze data.
    [issue](https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/37)

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.10] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.9] - 2022-09-21

### Added

-   Migration scripts for database tables

### Changed

-   new schema for db tables
-   new PK for table wazett_subroutes

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.8] - 2022-06-30

### Fixed

-   validation schema: route type changed to number | string ([WazeTT#33](https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/33))

## [1.0.7] - 2022-05-25

### Fixed

-   schema: route-id changed in every table to type int8 ([WazeTT#28](https://gitlab.com/operator-ict/golemio/projekty/oict/d0207.-waze-dojezdove-doby/-/issues/28))

## [1.0.6] - 2022-05-18

### Fixed

-   validation fix: wazersCount can contain also float numbers, routes/id is string with number pattern, routes/type is number

## [1.0.5] - 2022-01-18

### Added

-   initial

