# Implementační dokumentace modulu _WazeTT_

## Záměr

Modul slouží k ukladání WazeTT dat

## Vstupní data

## Zpracování dat / transformace

Interní RabbitMQ fronty jsou popsány v [AsyncAPI](./asyncapi.yaml).

Všechny tabulky jsou ve schématu `wazett`

### _WazeTTWorker_

WazeTT modul má jednoho workera, který se stará o ukládání dat

#### _task: FetchAndSaveDataToDBTask_

Fetchuje data z pole datových zdrojů a ukládá data do tabulek `wazett_feeds`, `wazett_jams_stats`, `wazett_route_lives`, `wazett_routes`,`wazett_subroute_lives`, `wazett_subroutes`

-   vstupní rabbitmq fronta
    -   nazev: dataplatform.wazett.fetchAndSaveDataToDB
    -   [parametry]
    -   žádné
-   datové zdroje
    -   [WazettDataSourceFactory](../src/integration-engine/datasources/WazeTTDataSourceFactory.ts)
-   transformace
    -   [Feed Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsFeed.ts)
    -   [Routes Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsRoutes.ts)
    -   [SubRoutes Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsSubroutes.ts)
    -   [Route Lives Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsRouteLives.ts)
    -   [Subroute Lives Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsSubRouteLives.ts)
-   data modely
    -   [FeedsRepository](../src/integration-engine/repositories/FeedsRepository.ts)
    -   [JamsStatsRepository](../src/integration-engine/repositories/JamsStatsRepository.ts)
    -   [RouteLivesRepository](../src/integration-engine/repositories/RouteLivesRepository.ts)
    -   [RoutesRepository](../src/integration-engine/repositories/RoutesRepository.ts)
    -   [SubRouteLivesRepository](../src/integration-engine/repositories/SubRouteLivesRepository.ts)
    -   [SubRoutesRepository](../src/integration-engine/repositories/SubRoutesRepository.ts)

#### _task: SaveS3DataToDBTask_

ukládá data z S3 do tabulek `wazett_feeds`, `wazett_jams_stats`, `wazett_route_lives`, `wazett_routes`,`wazett_subroute_lives`, `wazett_subroutes`

-   vstupní rabbitmq fronta
    -   název: dataplatform.wazett.saveS3DataToDB
    -   [parametry]
    -   žádné
-   datové zdroje
    -   žádne
-   transformace
    -   [Feed Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsFeed.ts)
    -   [Routes Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsRoutes.ts)
    -   [SubRoutes Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsSubroutes.ts)
    -   [Route Lives Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsRouteLives.ts)
    -   [Subroute Lives Transformation](../src/integration-engine/wazeTTTransformations/WazeTTTransformationsSubRouteLives.ts)
-   data modely
    -   [FeedsRepository](../src/integration-engine/repositories/FeedsRepository.ts)
    -   [JamsStatsRepository](../src/integration-engine/repositories/JamsStatsRepository.ts)
    -   [RouteLivesRepository](../src/integration-engine/repositories/RouteLivesRepository.ts)
    -   [RoutesRepository](../src/integration-engine/repositories/RoutesRepository.ts)
    -   [SubRouteLivesRepository](../src/integration-engine/repositories/SubRouteLivesRepository.ts)
    -   [SubRoutesRepository](../src/integration-engine/repositories/SubRoutesRepository.ts)

## Output API

### Obecné
