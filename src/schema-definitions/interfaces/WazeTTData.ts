export interface IWazeTTFeed {
    id: string;
    sourceId: number;
    broadcasterId: string;
    areaName: string;
    bbox: Record<string, unknown>;
    name: string;
    isMetric: boolean;
    updateTime?: number;
    usersOnJams: IWazerJam[];
    routes: IWazeTTRoute[];
    lengthOfJams: IJamLength[];
}

interface IWazerJam {
    wazersCount: number | string;
    jamLevel: number | string;
}

interface IJamLength {
    jamLevel: number;
    jamLength: number;
}

export interface IWazeTTRoute {
    toName: string;
    historicTime?: number | string;
    line?: IGeoPoint[];
    bbox: Record<string, unknown>;
    name: string;
    fromName: string;
    id: string;
    length: number;
    jamLevel: number;
    time: number;
    type: string;
    jams?: unknown[];
    subRoutes: IWazeTTSubRoute[];
}

export interface IGeoPoint {
    x: number; // Longitude
    y: number; // Latitude
}

export interface IWazeTTSubRoute {
    toName: string;
    historicTime: number;
    line?: IGeoPoint[];
    bbox: Record<string, unknown>;
    fromName: string;
    length: number;
    jamLevel: number;
    time: number;
    jams?: unknown[];
    name: string;
    id: string;
    type: string;
}
