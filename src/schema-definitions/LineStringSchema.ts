export const LineStringSchema = {
    type: "object",
    required: ["type", "coordinates"],
    properties: {
        type: {
            type: "string",
            enum: ["LineString"],
        },
        coordinates: {
            type: "array",
            items: {
                type: "array",
                minItems: 2,
                items: {
                    type: "number",
                },
            },
        },
    },
};
