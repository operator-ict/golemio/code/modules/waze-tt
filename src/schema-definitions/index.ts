import Sequelize from "@golemio/core/dist/shared/sequelize";
import { LineStringSchema } from "./LineStringSchema";
// SDMA = Sequelize DefineModelAttributes

const datasourceWazeTTJsonSchema = {
    type: "object",
    properties: {
        usersOnJams: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    wazersCount: { type: ["number", "string"] },
                    jamLevel: { type: ["integer", "string"] },
                },
            },
        },
        routes: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    toName: { type: "string" },
                    historicTime: { type: ["integer", "string"] },
                    line: {
                        type: "array",
                        items: {
                            type: "object",
                        },
                    },
                    bbox: {
                        type: "object",
                    },
                    name: { type: "string" },
                    fromName: { type: "string" },
                    length: { type: "number" },
                    jamLevel: { type: "number" },
                    id: { type: "string", pattern: "^[0-9]+$" },
                    time: { type: "integer" },
                    type: { type: ["integer", "string"] },
                    jams: { type: "array" },
                },
                required: ["id"],
            },
        },
        irregularities: { type: "array" },
        broadcasterId: { type: "string" },
        areaName: { type: "string" },
        bbox: {
            type: "object",
        },
        name: { type: "string" },
        isMetric: { type: "boolean" },
        restrictions: { type: "object" },
        lengthOfJams: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    jamLevel: { type: "integer" },
                    jamLength: { type: "integer" },
                },
                required: ["jamLevel", "jamLength"],
            },
        },
        updateTime: { type: "number" },
    },
    required: ["usersOnJams", "broadcasterId", "areaName", "bbox", "name", "isMetric"],
};

const outputFeedsJsonSchema = {
    type: "object",
    properties: {
        area_name: { type: "string" },
        bbox: { type: "object" },
        broadcaster_id: { type: "string" },
        id: { type: "number" },
        ismetric: { type: "boolean" },
        name: { type: "string" },
    },
    required: ["area_name", "bbox", "broadcaster_id", "ismetric", "name"],
};

const outputJamsStatsJsonSchema = {
    type: "object",
    properties: {
        feed_id: { type: "number" },
        jam_level: { type: "number" },
        length_of_jams: { type: "number" },
        update_time: { type: "object", required: ["toISOString"] },
        wazers_count: { type: "number" },
    },
    required: ["feed_id", "jam_level", "update_time"],
};

const outputRoutesJsonSchema = {
    type: "object",
    properties: {
        bbox: { type: "object" },
        feed_id: { type: "number" },
        from_name: { type: "string" },
        id: { type: "number" },
        last_update: { type: "object", required: ["toISOString"] },
        line: LineStringSchema,
        name: { type: "string" },
        to_name: { type: "string" },
    },
    required: ["id"],
};

const outputSubRoutesJsonSchema = {
    type: "object",
    properties: {
        from_name: { type: "string" },
        line_md5: { type: "string" },
        line: LineStringSchema,
        route_id: { type: "string" },
        to_name: { type: "string" },
    },
    required: ["line_md5", "route_id"],
};
const outputRouteLivesJsonSchema = {
    type: "object",
    properties: {
        historic_time: { type: "number" },
        jam_level: { type: "number" },
        length: { type: "number" },
        route_id: { type: "string" },
        time: { type: "number" },
        update_time: { type: "object", required: ["toISOString"] },
    },
    required: ["route_id", "update_time"],
};
const outputSubRouteLivesJsonSchema = {
    type: "object",
    properties: {
        historic_time: { type: "number" },
        jam_level: { type: "number" },
        length: { type: "number" },
        route_id: { type: "string" },
        subroute_line_md5: { type: "string" },
        time: { type: "number" },
        update_time: { type: "object", required: ["toISOString"] },
    },
    required: ["route_id", "subroute_line_md5", "update_time"],
};

const outputFeedsSDMA: Sequelize.ModelAttributes<any> = {
    area_name: Sequelize.TEXT,
    bbox: Sequelize.JSONB,
    broadcaster_id: Sequelize.TEXT,
    id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    ismetric: Sequelize.BOOLEAN,
    name: Sequelize.TEXT,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputJamsStatsSDMA: Sequelize.ModelAttributes<any> = {
    feed_id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    jam_level: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    length_of_jams: Sequelize.INTEGER,
    update_time: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.DATE,
    },
    wazers_count: Sequelize.NUMBER,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputRoutesSDMA: Sequelize.ModelAttributes<any> = {
    bbox: Sequelize.JSONB,
    feed_id: Sequelize.INTEGER,
    from_name: Sequelize.TEXT,
    id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    last_update: Sequelize.DATE,
    line: Sequelize.GEOMETRY("LINESTRING"),
    name: Sequelize.TEXT,
    to_name: Sequelize.TEXT,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputSubRoutesSDMA: Sequelize.ModelAttributes<any> = {
    line_md5: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
    },
    route_id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
    },
    from_name: Sequelize.TEXT,
    line: Sequelize.GEOMETRY("LINESTRING"),
    to_name: Sequelize.TEXT,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputRouteLivesSDMA: Sequelize.ModelAttributes<any> = {
    historic_time: Sequelize.INTEGER,
    jam_level: Sequelize.INTEGER,
    length: Sequelize.INTEGER,
    route_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    time: Sequelize.INTEGER,
    update_time: {
        primaryKey: true,
        type: Sequelize.DATE,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputSubRouteLivesSDMA: Sequelize.ModelAttributes<any> = {
    route_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    subroute_line_md5: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
    },
    historic_time: Sequelize.INTEGER,
    jam_level: Sequelize.INTEGER,
    length: Sequelize.INTEGER,
    time: Sequelize.INTEGER,
    update_time: {
        primaryKey: true,
        type: Sequelize.DATE,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const forExport = {
    pgSchema: "wazett",
    datasourceWazeTTJsonSchema,
    feeds: {
        name: "WazeTTFeeds",
        outputJsonSchemaDefinition: outputFeedsJsonSchema,
        outputSequelizeAttributes: outputFeedsSDMA,
        pgTableName: "wazett_feeds",
    },
    jamsStats: {
        name: "WazeTTJamsStats",
        outputJsonSchemaDefinition: outputJamsStatsJsonSchema,
        outputSequelizeAttributes: outputJamsStatsSDMA,
        pgTableName: "wazett_jams_stats",
    },
    name: "WazeTT",
    routeLives: {
        name: "WazeTTRouteLives",
        outputJsonSchemaDefinition: outputRouteLivesJsonSchema,
        outputSequelizeAttributes: outputRouteLivesSDMA,
        pgTableName: "wazett_route_lives",
    },
    routes: {
        name: "WazeTTRoutes",
        outputJsonSchemaDefinition: outputRoutesJsonSchema,
        outputSequelizeAttributes: outputRoutesSDMA,
        pgTableName: "wazett_routes",
    },
    subRouteLives: {
        name: "WazeTTSubRouteLives",
        outputJsonSchemaDefinition: outputSubRouteLivesJsonSchema,
        outputSequelizeAttributes: outputSubRouteLivesSDMA,
        pgTableName: "wazett_subroute_lives",
    },
    subRoutes: {
        name: "WazeTTSubRoutes",
        outputJsonSchemaDefinition: outputSubRoutesJsonSchema,
        outputSequelizeAttributes: outputSubRoutesSDMA,
        pgTableName: "wazett_subroutes",
    },
};

export { forExport as WazeTT };
