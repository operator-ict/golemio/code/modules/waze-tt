import { WazeTT } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RouteLivesRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            WazeTT.routeLives.name + "Model",
            {
                outputSequelizeAttributes: WazeTT.routeLives.outputSequelizeAttributes,
                pgSchema: WazeTT.pgSchema,
                pgTableName: WazeTT.routeLives.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WazeTT.routeLives.name + "ModelJSONSchemaValidator",
                WazeTT.routeLives.outputJsonSchemaDefinition
            )
        );
    }
}
