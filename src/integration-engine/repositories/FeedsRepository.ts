import { WazeTT } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class FeedsRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            WazeTT.feeds.name + "Model",
            {
                outputSequelizeAttributes: WazeTT.feeds.outputSequelizeAttributes,
                pgSchema: WazeTT.pgSchema,
                pgTableName: WazeTT.feeds.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WazeTT.feeds.name + "ModelJSONSchemaValidator", WazeTT.feeds.outputJsonSchemaDefinition)
        );
    }
}
