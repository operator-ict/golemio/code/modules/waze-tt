import { WazeTT } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RoutesRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            WazeTT.routes.name + "Model",
            {
                outputSequelizeAttributes: WazeTT.routes.outputSequelizeAttributes,
                pgSchema: WazeTT.pgSchema,
                pgTableName: WazeTT.routes.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WazeTT.routes.name + "ModelJSONSchemaValidator", WazeTT.routes.outputJsonSchemaDefinition)
        );
    }
}
