import { WazeTT } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class JamsStatsRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            WazeTT.jamsStats.name + "Model",
            {
                outputSequelizeAttributes: WazeTT.jamsStats.outputSequelizeAttributes,
                pgSchema: WazeTT.pgSchema,
                pgTableName: WazeTT.jamsStats.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                WazeTT.jamsStats.name + "ModelJSONSchemaValidator",
                WazeTT.jamsStats.outputJsonSchemaDefinition
            )
        );
    }
}
