import { WazeTT } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class SubRouteLivesRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            WazeTT.subRouteLives.name + "Model",
            {
                outputSequelizeAttributes: WazeTT.subRouteLives.outputSequelizeAttributes,
                pgSchema: WazeTT.pgSchema,
                pgTableName: WazeTT.subRouteLives.pgTableName,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(
                WazeTT.subRouteLives.name + "ModelJSONSchemaValidator",
                WazeTT.subRouteLives.outputJsonSchemaDefinition
            )
        );
    }
}
