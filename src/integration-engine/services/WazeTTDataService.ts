import { WazeTT } from "#sch";
import { DataSource } from "@golemio/core/dist/integration-engine";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import {
    WazeTTTransformationsFeed,
    WazeTTTransformationsRoutes,
    WazeTTTransformationsRouteLives,
    WazeTTTransformationsSubRouteLives,
    WazeTTTransformationsSubRoutes,
} from "#ie/wazeTTTransformations";
import { FeedsRepository } from "#ie/repositories/FeedsRepository";
import { RoutesRepository } from "#ie/repositories/RoutesRepository";
import { SubRoutesRepository } from "#ie/repositories/SubRoutesRepository";
import { RouteLivesRepository } from "#ie/repositories/RouteLivesRepository";
import { SubRouteLivesRepository } from "#ie/repositories/SubRouteLivesRepository";
import { JamsStatsRepository } from "#ie/repositories/JamsStatsRepository";
import ISubRoute from "#ie/wazeTTTransformations/interfaces/ISubRoute";
import { IWazeTTFeed, IWazeTTRoute, IWazeTTSubRoute } from "#sch/interfaces/WazeTTData";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";

@injectable()
export class WazeTTDataService {
    private static readonly MAX_RETRIES = 10;
    private static readonly RETRY_DELAY_MS = 5000;
    constructor(
        @inject(ModuleContainerToken.WazeTTTransformationsFeed)
        private transformationFeeds: WazeTTTransformationsFeed,
        @inject(ModuleContainerToken.WazeTTTransformationsRoutes)
        private transformationRoutes: WazeTTTransformationsRoutes,
        @inject(ModuleContainerToken.WazeTTTransformationsRouteLives)
        private transformationRouteLives: WazeTTTransformationsRouteLives,
        @inject(ModuleContainerToken.WazeTTTransformationsSubRouteLives)
        private transformationSubRouteLives: WazeTTTransformationsSubRouteLives,
        @inject(ModuleContainerToken.WazeTTTransformationsSubRoutes)
        private transformationSubRoutes: WazeTTTransformationsSubRoutes,
        @inject(ModuleContainerToken.FeedsRepository)
        private modelFeeds: FeedsRepository,
        @inject(ModuleContainerToken.RoutesRepository)
        private modelRoutes: RoutesRepository,
        @inject(ModuleContainerToken.SubRoutesRepository)
        private modelSubRoutes: SubRoutesRepository,
        @inject(ModuleContainerToken.RouteLivesRepository)
        private modelRouteLives: RouteLivesRepository,
        @inject(ModuleContainerToken.SubRouteLivesRepository)
        private modelSubRouteLives: SubRouteLivesRepository,
        @inject(ModuleContainerToken.JamsStatsRepository)
        private modelJamsStats: JamsStatsRepository,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {}

    private delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

    public fetchFromSingleResource = async (singleDataSourceWazeTT: DataSource, sourceId: number): Promise<void> => {
        let data;
        let tryCount = 0;

        while (tryCount < WazeTTDataService.MAX_RETRIES) {
            try {
                data = await singleDataSourceWazeTT.getAll();
                break;
            } catch (error) {
                tryCount++;
                if (tryCount >= WazeTTDataService.MAX_RETRIES) {
                    this.logger.error(
                        `${WazeTT.name}: Failed to fetch data after ${WazeTTDataService.MAX_RETRIES} attempts.`,
                        error
                    );
                    return;
                }
                this.logger.warn(`${WazeTT.name}: Error while getting data. Next try #${tryCount} in 5 sec...`);
                await this.delay(WazeTTDataService.RETRY_DELAY_MS);
            }
        }

        if (data) {
            data.sourceId = sourceId;
            await this.saveFromSingleResource(data);
        }
    };

    /**
     * This method splits the object into individual table records and saves them to the database
     * @param {object} data object of one feed, where data.sourceId - is the sequence number
     * of the source address in the array in the source configuration file
     */
    public saveFromSingleResource = async (data: IWazeTTFeed): Promise<void> => {
        await this.saveFeeds(data);
        const feedId = data.sourceId;
        // updateTime for route, jams_stats, route_lives, subroute_lives tables
        const updateTime = new Date(typeof data.updateTime === "number" ? data.updateTime : Number(data.updateTime));

        for (const route of data.routes) {
            await this.saveRoute(route, feedId, updateTime);
            await this.saveRouteLives(route, route.id, updateTime);

            if (route.hasOwnProperty("subRoutes")) {
                for (const subroute of route.subRoutes) {
                    await this.saveSubRoute(subroute, route.id, updateTime);
                }
            }
        }

        await this.saveJams(data, feedId, updateTime);
    };

    private async saveFeeds(data: IWazeTTFeed) {
        const transformedFeed = await this.transformationFeeds.transform(data);
        await this.modelFeeds.save(transformedFeed);
    }

    private async saveRoute(route: IWazeTTRoute, feedId: number, updateTime: Date) {
        const transformedRoute = await this.transformationRoutes.transform(route);
        transformedRoute.feed_id = feedId;
        transformedRoute.last_update = updateTime;

        await this.modelRoutes.save(transformedRoute);
    }

    private async saveRouteLives(route: IWazeTTRoute, routeId: string, updateTime: Date) {
        const transformedRouteLive = await this.transformationRouteLives.transform(route);
        transformedRouteLive.route_id = routeId;
        transformedRouteLive.update_time = updateTime;

        await this.modelRouteLives.save(transformedRouteLive);
    }

    private async saveSubRoute(subroute: IWazeTTSubRoute, routeId: string, updateTime: Date) {
        const transformedSubRoute = await this.transformationSubRoutes.transform(subroute);
        if (transformedSubRoute) {
            transformedSubRoute.route_id = routeId;
            await this.modelSubRoutes.save(transformedSubRoute);
            await this.saveSubRouteLives(subroute, updateTime, routeId, transformedSubRoute);
        }
    }

    private async saveSubRouteLives(
        subroute: IWazeTTSubRoute,
        updateTime: Date,
        routeId: string,
        transformedSubRoute: ISubRoute
    ) {
        const transformedSubRouteLive = await this.transformationSubRouteLives.transform(subroute);
        transformedSubRouteLive.update_time = updateTime;
        transformedSubRouteLive.route_id = routeId;
        transformedSubRouteLive.subroute_line_md5 = transformedSubRoute.line_md5;

        await this.modelSubRouteLives.save(transformedSubRouteLive);
    }

    private async saveJams(data: IWazeTTFeed, feedId: number, updateTime: Date) {
        const jamsStatsArr = data.usersOnJams.map((element) => {
            const matchedJam = data.lengthOfJams.find((item) => item.jamLevel - 1 === element.jamLevel);
            return {
                feed_id: feedId,
                jam_level: Number(element.jamLevel),
                length_of_jams: matchedJam?.jamLength ?? 0,
                wazers_count: Number(element.wazersCount),
                update_time: updateTime,
            };
        });

        await Promise.all(jamsStatsArr.map((jamsStatsSet) => this.modelJamsStats.save(jamsStatsSet)));
    }
}
