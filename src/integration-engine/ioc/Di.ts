import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainer";
import { FetchAndSaveDataToDBTask } from "#ie/workers/tasks/FetchAndSaveDataToDBTask";
import {
    WazeTTTransformationsFeed,
    WazeTTTransformationsRouteLives,
    WazeTTTransformationsRoutes,
    WazeTTTransformationsSubRouteLives,
    WazeTTTransformationsSubRoutes,
} from "#ie/wazeTTTransformations";
import { FeedsRepository } from "#ie/repositories/FeedsRepository";
import { RoutesRepository } from "#ie/repositories/RoutesRepository";
import { SubRoutesRepository } from "#ie/repositories/SubRoutesRepository";
import { RouteLivesRepository } from "#ie/repositories/RouteLivesRepository";
import { SubRouteLivesRepository } from "#ie/repositories/SubRouteLivesRepository";
import { JamsStatsRepository } from "#ie/repositories/JamsStatsRepository";
import { SaveS3DataToDBTask } from "#ie/workers/tasks";
import { WazeTTDataService } from "#ie/services/WazeTTDataService";
import { WazeTTDataSourceFactory } from "#ie/datasources/WazeTTDataSourceFactory";

//#region Initialization
const WazeTTContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();

//#region Repositories
WazeTTContainer.registerSingleton(ModuleContainerToken.FeedsRepository, FeedsRepository);
WazeTTContainer.registerSingleton(ModuleContainerToken.RoutesRepository, RoutesRepository);
WazeTTContainer.registerSingleton(ModuleContainerToken.SubRoutesRepository, SubRoutesRepository);
WazeTTContainer.registerSingleton(ModuleContainerToken.RouteLivesRepository, RouteLivesRepository);
WazeTTContainer.registerSingleton(ModuleContainerToken.SubRouteLivesRepository, SubRouteLivesRepository);
WazeTTContainer.registerSingleton(ModuleContainerToken.JamsStatsRepository, JamsStatsRepository);
//#endregion

//#region Services
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTDataService, WazeTTDataService);
//#endregion

//#region Tasks
WazeTTContainer.registerSingleton(ModuleContainerToken.FetchAndSaveDataToDBTask, FetchAndSaveDataToDBTask);
WazeTTContainer.registerSingleton(ModuleContainerToken.SaveS3DataToDBTask, SaveS3DataToDBTask);
//#endregion

//#region Transformations
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTTransformationsFeed, WazeTTTransformationsFeed);
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTTransformationsRoutes, WazeTTTransformationsRoutes);
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTTransformationsRouteLives, WazeTTTransformationsRouteLives);
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTTransformationsSubRoutes, WazeTTTransformationsSubRoutes);
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTTransformationsSubRouteLives, WazeTTTransformationsSubRouteLives);
//#endregion

//#region Datasources
WazeTTContainer.registerSingleton(ModuleContainerToken.WazeTTDataSourceFactory, WazeTTDataSourceFactory);

//#endregion

export { WazeTTContainer };
