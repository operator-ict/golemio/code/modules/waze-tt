const ModuleContainerToken = {
    /* Repositories */
    FeedsRepository: Symbol(),
    RoutesRepository: Symbol(),
    RouteLivesRepository: Symbol(),
    SubRouteLivesRepository: Symbol(),
    SubRoutesRepository: Symbol(),
    JamsStatsRepository: Symbol(),
    /* Services */
    WazeTTDataService: Symbol(),
    /* Transformations */
    WazeTTTransformationsFeed: Symbol(),
    WazeTTTransformationsRoutes: Symbol(),
    WazeTTTransformationsRouteLives: Symbol(),
    WazeTTTransformationsSubRoutes: Symbol(),
    WazeTTTransformationsSubRouteLives: Symbol(),
    /* Tasks */
    FetchAndSaveDataToDBTask: Symbol(),
    SaveS3DataToDBTask: Symbol(),
    /* DataSources */
    WazeTTDataSourceFactory: Symbol(),
};

export { ModuleContainerToken };
