import { WazeTTContainer } from "#ie/ioc/Di";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";
import { FetchAndSaveDataToDBTask } from "./tasks/FetchAndSaveDataToDBTask";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { SaveS3DataToDBTask } from "./tasks";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";

export class WazeTTWorker extends AbstractWorker {
    protected readonly name = "WazeTT";
    constructor() {
        super();

        // Register tasks
        this.registerTask(WazeTTContainer.resolve<FetchAndSaveDataToDBTask>(ModuleContainerToken.FetchAndSaveDataToDBTask));
        this.registerTask(WazeTTContainer.resolve<SaveS3DataToDBTask>(ModuleContainerToken.SaveS3DataToDBTask));
    }

    public registerTask = (task: AbstractTask<any> | AbstractTaskJsonSchema<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
