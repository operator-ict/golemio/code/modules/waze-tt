import { WazeTT } from "#sch";
import { AbstractEmptyTask, DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { config } from "@golemio/core/dist/integration-engine/config";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { WazeTTDataService } from "#ie/services/WazeTTDataService";
import { WazeTTDataSourceFactory } from "#ie/datasources/WazeTTDataSourceFactory";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class FetchAndSaveDataToDBTask extends AbstractEmptyTask {
    public readonly queueName = "fetchAndSaveDataToDB";
    public readonly queueTtl = 4 * 60 * 1000;
    private dataSourcesWazeTTArr: DataSource[];

    constructor(
        @inject(ModuleContainerToken.WazeTTDataService)
        private wazeTTDataService: WazeTTDataService,
        @inject(ModuleContainerToken.WazeTTDataSourceFactory)
        private dataSourceFactory: WazeTTDataSourceFactory
    ) {
        super(WazeTT.name.toLowerCase());
        this.dataSourcesWazeTTArr = this.dataSourceFactory.getDataSources();
    }

    protected async execute(): Promise<void> {
        try {
            const fetchTasksArr: Array<Promise<any>> = [];
            for (let i = 0; i < this.dataSourcesWazeTTArr.length; i++) {
                const singleDataSourceWazeTT = this.dataSourcesWazeTTArr[i];
                fetchTasksArr.push(this.wazeTTDataService.fetchFromSingleResource(singleDataSourceWazeTT, i + 1));
            }

            await Promise.all(fetchTasksArr);
        } catch (error) {
            throw new GeneralError("Error while processing WazeTT data ", "FetchAndSaveDataToDBTask", error, 422);
        }
    }
}
