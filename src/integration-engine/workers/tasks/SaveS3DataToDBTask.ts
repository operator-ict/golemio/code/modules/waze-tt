import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { WazeTTDataService } from "#ie/services/WazeTTDataService";
import { WazeTT } from "#sch";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IWazeTTFeed } from "#sch/interfaces/WazeTTData";

@injectable()
export class SaveS3DataToDBTask extends AbstractTaskJsonSchema<IWazeTTFeed> {
    public readonly queueName = "saveS3DataToDB";
    public readonly queueTtl = 4 * 60 * 1000;
    protected schema: JSONSchemaValidator;
    constructor(
        @inject(ModuleContainerToken.WazeTTDataService)
        private wazeTTDataService: WazeTTDataService
    ) {
        super(WazeTT.name.toLowerCase());
        this.schema = new JSONSchemaValidator("datasourceWazeTTJsonSchema", WazeTT.datasourceWazeTTJsonSchema);
    }

    protected async execute(msg: IWazeTTFeed): Promise<void> {
        try {
            if (!msg.sourceId) {
                throw new GeneralError("Missing sourceId in WazeTT data", this.constructor.name);
            }
            await this.wazeTTDataService.saveFromSingleResource(msg);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            } else {
                throw new GeneralError("Error while processing WazeTT data ", this.constructor.name, error);
            }
        }
    }
}
