import { WazeTT } from "#sch/index";
import { IWazeTTRoute } from "#sch/interfaces/WazeTTData";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class WazeTTTransformationsRouteLives extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = WazeTT.name;
    }

    public transform = async (route: IWazeTTRoute): Promise<any | any[]> => {
        return this.transformElement(route);
    };

    protected transformElement = async (route: IWazeTTRoute): Promise<any | any[]> => {
        if (!route) {
            log.warn(`${this.name}: Data source returned unexpected data.`);
            return {};
        }

        const result = {
            historic_time: route.historicTime,
            jam_level: route.jamLevel,
            length: route.length,
            time: route.time,
        };

        return result;
    };
}
