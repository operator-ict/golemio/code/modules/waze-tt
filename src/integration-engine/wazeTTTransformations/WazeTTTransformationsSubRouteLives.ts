import { WazeTT } from "#sch/index";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import ISubRouteLives from "./interfaces/ISubRouteLives";
import { IWazeTTSubRoute } from "#sch/interfaces/WazeTTData";

export class WazeTTTransformationsSubRouteLives extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = WazeTT.name;
    }

    public transform = async (subRoute: IWazeTTSubRoute): Promise<ISubRouteLives> => {
        return this.transformElement(subRoute);
    };

    protected transformElement = async (subRoute: IWazeTTSubRoute): Promise<ISubRouteLives> => {
        const result: ISubRouteLives = {
            historic_time: subRoute.historicTime,
            jam_level: subRoute.jamLevel,
            length: subRoute.length,
            time: subRoute.time,
        };

        return result;
    };
}
