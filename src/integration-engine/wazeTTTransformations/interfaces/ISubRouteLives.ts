export default interface ISubRouteLives {
    historic_time: number;
    jam_level: number;
    length: number;
    time: number;
    update_time?: Date;
    route_id?: string;
    subroute_line_md5?: string;
}
