import { LineString } from "geojson";

export default interface ISubRoute {
    line_md5?: string;
    route_id?: string;
    line: LineString;
    from_name: string;
    to_name: string;
}
