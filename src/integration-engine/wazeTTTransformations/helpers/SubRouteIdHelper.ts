import crypto from "crypto";
import ISubRoute from "../interfaces/ISubRoute";

export default class SubrouteIdHelper {
    public static GenerateLineUuid(subroute: ISubRoute) {
        if (this.isValidSubroute(subroute))
            throw new Error(`SubrouteIdHelper: Unable to generate subroute uuid '${JSON.stringify(subroute)}'.`);
        const md5 = this.getMd5(this.geomLineToText(subroute.line));
        const uuid = this.formatAsUuid(md5);

        return uuid;
    }

    private static isValidSubroute(subroute: ISubRoute) {
        return (
            !subroute.line ||
            !subroute.line.type ||
            !(subroute.line.coordinates?.length > 0) ||
            subroute.line.type !== "LineString"
        );
    }

    private static geomLineToText(line: { coordinates: number[][]; type: string }) {
        return line.type.toUpperCase().concat("(", line.coordinates.map((coord) => coord.join(" ")).join(","), ")");
    }

    private static getMd5(text: string) {
        return crypto.createHash("md5").update(text).digest("hex");
    }

    private static formatAsUuid(text: string) {
        //a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
        if (text.length !== 32) throw new Error(`SubrouteIdHelper: Unable to format string '${text}' as uuid.`);

        return `${text.slice(0, 8)}-${text.slice(8, 12)}-${text.slice(12, 16)}-${text.slice(16, 20)}-${text.slice(20)}`;
    }
}
