import { WazeTT } from "#sch/index";
import { IWazeTTRoute } from "#sch/interfaces/WazeTTData";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class WazeTTTransformationsRoutes extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = WazeTT.name;
    }

    public transform = async (route: IWazeTTRoute): Promise<any | any[]> => {
        return this.transformElement(route);
    };

    protected transformElement = async (route: IWazeTTRoute): Promise<any | any[]> => {
        if (!Array.isArray(route?.line)) {
            log.warn(`${this.name}: Data source returned unexpected data.`);
            return {};
        }

        const coordinatesArr = route.line.map((elem) => [elem.x, elem.y]);

        const result = {
            bbox: route.bbox,
            from_name: route.fromName,
            id: Number.parseInt(route.id),
            line: {
                coordinates: coordinatesArr,
                type: "LineString",
            },
            name: route.name,
            to_name: route.toName,
        };

        return result;
    };
}
