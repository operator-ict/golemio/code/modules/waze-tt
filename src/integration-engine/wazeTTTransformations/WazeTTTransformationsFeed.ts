import { WazeTT } from "#sch/index";
import { IWazeTTFeed } from "#sch/interfaces/WazeTTData";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class WazeTTTransformationsFeed extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = WazeTT.name;
    }

    public transform = async (data: IWazeTTFeed): Promise<any | any[]> => {
        return this.transformElement(data);
    };

    protected transformElement = async (data: IWazeTTFeed): Promise<any | any[]> => {
        if (!data) {
            log.warn(`${this.name}: Data source returned empty data.`);
            return {};
        }

        const results = {
            id: data.sourceId,
            area_name: data.areaName,
            bbox: data.bbox,
            broadcaster_id: data.broadcasterId,
            ismetric: data.isMetric,
            name: data.name,
        };

        return results;
    };
}
