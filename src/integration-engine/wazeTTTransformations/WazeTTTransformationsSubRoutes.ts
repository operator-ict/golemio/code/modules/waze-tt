import { WazeTT } from "#sch/index";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import SubrouteIdHelper from "./helpers/SubRouteIdHelper";
import ISubRoute from "./interfaces/ISubRoute";
import { IWazeTTSubRoute } from "#sch/interfaces/WazeTTData";

export class WazeTTTransformationsSubRoutes extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = WazeTT.name;
    }

    public transform = async (subRoute: IWazeTTSubRoute): Promise<undefined | ISubRoute> => {
        return this.transformElement(subRoute);
    };

    protected transformElement = async (subRoute: IWazeTTSubRoute): Promise<undefined | ISubRoute> => {
        if (!Array.isArray(subRoute.line)) {
            log.warn(`${this.name}: Data source returned unexpected data: ${JSON.stringify(subRoute)}`);
            return undefined;
        }

        const coordinatesArr = subRoute.line.map((elem) => [elem.x, elem.y]);

        const result: ISubRoute = {
            from_name: subRoute.fromName,
            line: {
                coordinates: coordinatesArr,
                type: "LineString",
            },
            to_name: subRoute.toName,
        };

        result.line_md5 = SubrouteIdHelper.GenerateLineUuid(result);

        return result;
    };
}
