import { WazeTT } from "#sch";

import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, JSONDataTypeStrategy, ProtocolStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class WazeTTDataSourceFactory {
    private static DATASOURCE_NAME = "WazeTTDataSource";
    private readonly urls: string[];

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        const urlsObject = this.config.getValue<{ [index: string]: string }>("module.WazeTT");
        this.urls = Object.values(urlsObject);
    }

    public getDataSources(): DataSource[] {
        return this.urls.map((url: string) => this.getDataSource(url));
    }

    private getDataSource(dataSourceUrl: string): DataSource {
        return new DataSource(
            WazeTTDataSourceFactory.DATASOURCE_NAME,
            this.getProtocolStrategy(dataSourceUrl),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(WazeTTDataSourceFactory.DATASOURCE_NAME + "Validator", WazeTT.datasourceWazeTTJsonSchema)
        );
    }

    private getProtocolStrategy(dataSourceUrl: string): ProtocolStrategy {
        return new HTTPFetchProtocolStrategy({
            method: "GET",
            headers: {},
            url: dataSourceUrl,
        });
    }
}
