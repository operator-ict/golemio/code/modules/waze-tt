import amqplib from "amqplib";
import fs from "fs";
import path from "path";

const RABBIT_CONN = `rabbit conn string`;
const RABBIT_EXCHANGE_NAME = `local exchange name`;
const RABBIT_EXCHANGE_NAME_ALT = `paroubek_module-test-alt`;
const RABBIT_QUEUE_NAME = "workers.paroubek_module-test.wazett.saveS3DataToDB";

(async () => {
    const connection = await amqplib.connect(RABBIT_CONN);
    const channel = await connection.createChannel();

    await channel.assertExchange(RABBIT_EXCHANGE_NAME, "topic", {
        durable: false,
        alternateExchange: RABBIT_EXCHANGE_NAME_ALT,
    });

    const sendMessage = async (key: string, msg: string, options: object = {}): Promise<boolean> => {
        return channel.publish(RABBIT_EXCHANGE_NAME as string, key, Buffer.from(msg), { expiration: 10 * 60 * 1000 });
    };
    const folder = `C:\\Users\\OP3509\\Desktop\\Golemio\\Waze\\TT\\prague\\2022-05-10`;
    const sourceId = 4;
    //sourceId = 5; //barrandov
    //sourceId = 4; //prague
    //sourceId = 3; //stredocesky
    //sourceId = 6; //stredocesky 2
    try {
        await s3Manual(folder, sourceId, sendMessage, channel, connection);
    } finally {
        await channel.close();
        await connection.close();
    }
})();

async function s3Manual(
    folder: string,
    sourceId: number,
    sendMessage: (key: string, msg: string, options?: object) => Promise<boolean>,
    channel: amqplib.Channel,
    connection: amqplib.Connection
) {
    const files = fs.readdirSync(folder);
    console.log(`folder: ${folder}\r\nfiles:${files.length}`);
    try {
        for (const file of files) {
            const fileContent = JSON.parse(fs.readFileSync(path.join(folder, file)).toString("utf8"));
            fileContent.sourceId = sourceId;
            await sendMessage(RABBIT_QUEUE_NAME, JSON.stringify(fileContent));
        }
    } catch (err) {
        console.log(err);
    }
}
