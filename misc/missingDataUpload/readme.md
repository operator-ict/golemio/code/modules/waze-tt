# Missing data upload
This folder contains script manualImportS3data.ts, which can be used occasionaly to upload missing data in WazeTT project.

## Steps
1) Find time period for which are you missing data
2) Login to S3 and in bucket /golem-cron/ download relevant files from folders to separate folders locally:
    - waze-wfc-traffic-view-polygon-stredocesky-kraj
    - waze-wfc-traffic-view-polygon-stredocesky-kraj-2
    - waze-wfc-traffic-view-polygon-prague
    - waze-wfc-traffic-view-polygon-barrandovsky-most

3) Inside script set local values for RABBIT_CONN, RABBIT_EXCHANGE_NAME, RABBIT_EXCHANGE_NAME_ALT, RABBIT_QUEUE_NAME. Also ensure that amqp lib is avaiable / installed.
4) In separate window start integration engine connected to local infrastructure.
5) Setup first folder in script on line 22, where missing data are stored locally and also setup proper sourceid. Current specification can be found in database table wazett_feeds.
6) Run the script e.g. with extension code runner.
7) Ensure that everything went succefully, check for errors in integration engine or new messages in rabbit dead queue.
8) If everything went succefully connect local integration engine with stage database or connect script with stage rabbit.
9) Run the script again.
10) Repeat steps 5 to 9 - it is important to check localy that everything goes well. Some data may be invalid.
